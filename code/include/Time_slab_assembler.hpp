#ifndef Time_slab_assembler_hpp
#define Time_slab_assembler_hpp

#include "utopia.hpp"
#include "utopia_fe_base.hpp"

class Time_slab_assembler{

    typedef std::shared_ptr<utopia::USparseMatrix> Matrix;

public:
    // set time step size and time order q
    Time_slab_assembler() : time_step(1.), size_time_slab(1){}
    Time_slab_assembler(const double time_step_, const int size_time_slab_ = 1)
        : time_step(time_step_), size_time_slab(size_time_slab_){}

    void assemble();

    // getters
    inline Matrix get_mass()           const{ return mass;           }
    inline Matrix get_stiff()          const{ return stiff;          }
    inline Matrix get_coupling()       const{ return coupling;       }
    inline utopia::SizeType get_size() const{ return size_time_slab; }
    inline double get_time_step()      const{ return time_step;      }
    inline bool get_coupling_need_stiffness_() const{ return coupling_need_stiffness_; }

protected:

    Matrix mass;
    Matrix stiff;
    Matrix coupling; // N

    double time_step;
    utopia::SizeType size_time_slab;

    bool coupling_need_stiffness_ = false;
};

#endif



