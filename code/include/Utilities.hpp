#ifndef Utilities_hpp
#define Utilities_hpp

#include "utopia.hpp"
#include "Space_time_assembler.hpp"
#include "Space_assembler.hpp"
#include "Laplace_assembler.hpp"
#include "libmesh/parallel_mesh.h"
#include "libmesh/nemesis_io.h"
#include "libmesh/exodusII_io.h"
#include "libmesh/mesh_refinement.h"
#include "libmesh/mesh_tools.h"
#include "moonolith_communicator.hpp"
#include "utopia_assemble_volume_transfer.hpp"

#include <fstream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>

class Space_time_assembler;

void print_local_sizes(const Mat &M);
void print_global_sizes(const Mat &M);
void save_mat(Mat &m, const char * filename, const char * name);
void save_vec(Vec &m, const char * filename, const char * name);
void save_solution(const Space_time_assembler &space_time_op, const utopia::UVector &solution, const std::string &path = "../../output/mg_solution.e", const bool parallel_out = true);
void save_space_solution(const Space_assembler &space_op, utopia::UVector &solution, const std::string &path);
void save_dofs_coord(Space_assembler &s_ass, std::shared_ptr<libMesh::DistributedMesh> mesh, const std::string filename = "coord.txt");

void set_zero_at_constraint_rows(libMesh::DofMap &dof_map, utopia::USparseMatrix &mat);

void extract_nth_time(const Space_time_assembler &st_op, const utopia::UVector &solution, utopia::UVector &nth_time_sol, const int nth_time_step, const int time_slab);
void insert_nth_time(const Space_time_assembler &st_op, utopia::UVector &space_time_sol, const utopia::UVector &space_sol, const int nth_time_step, const int time_slab);
void insert_nth_time_block(const Space_time_assembler &st_op, utopia::USparseMatrix &space_time_mat, const utopia::USparseMatrix &block, const int nth_time_step);

void extract_final_time(Space_time_assembler &st_op, utopia::UVector &solution, utopia::UVector &final_time);
void impose_initial_condition(Space_time_assembler &st_op, utopia::UVector &solution);
void copy_solution_to_next_time_block(Space_time_assembler  &st_op, utopia::UVector &final_time, utopia::UVector &new_solution);

void from_space_to_space_time(const Space_time_assembler &st_ass, const utopia::UVector &space_sol, utopia::UVector &space_time_sol);
void eikonal_to_space_time(Space_time_assembler &st_ass, utopia::UVector &eikonal_sol, utopia::UVector &space_time_sol, double u_min, double u_max);
int count_max_nz(const Mat &A);

void set_gaussian_forcing(const Laplace_assembler &space_op, utopia::UVector &result,
						const double u_max, const double u_min, const double sigma = 0.1,
						const double x0 = 0, const double y0 = 0, const double z0 = 0);

std::vector<int> generate_distribution(const int size_A, const int size);

//void write_time_block(Space_time_assembler &space_time_op, utopia::UVector &solution);

void coupling_space(const libMesh::LibMeshInit &init, Space_assembler &fine_op, Space_assembler &coarse_op, utopia::USparseMatrix &interp_op,
	const bool transfer_residual, const bool use_interpolation_ = false);
PetscErrorCode coupling_time(const int time_steps_fine, const int time_steps_coarse, Mat &I);

class SpaceTimeIO {
public:
	void write_time_block(Space_time_assembler &space_time_op, utopia::UVector &solution, const bool parallel_out = true);

	SpaceTimeIO(libMesh::MeshBase &mesh, const std::string &path)
	: n_steps(0), out(mesh), path(path)
	{
	}

	void clear()
	{
		n_steps = 0;
	}

private:
	long n_steps;
	libMesh::Nemesis_IO  out;
	// libMesh::ExodusII_IO out;
	std::string path;
};


// TODO maybe function form dg to "cg" and viceversa should be implemented!
#endif

// utopia::Chrono c;
// c.start();
// c.stop();
// utopia::mpi_world_barrier();
// if(utopia::mpi_world_rank() == 0) {
//     std::cout << "Space time operator assembly: " << c << std::endl;
// }






