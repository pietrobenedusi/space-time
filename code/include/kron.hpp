#ifndef KRON_H    
#define KRON_H

#include <petsc.h>
#include <vector>
#include "Utilities.hpp"

PetscErrorCode kron(const Mat &A, const Mat &B_in, Mat &AB, Mat* AB_structure = NULL);

#endif
