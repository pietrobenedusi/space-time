#ifndef Laplace_assembler_hpp
#define Laplace_assembler_hpp

#include "Space_assembler.hpp"
#include "utopia_TensorInterpolate.hpp"

class Laplace_assembler : public Space_assembler{

    typedef std::shared_ptr<utopia::USparseMatrix> Matrix;
    typedef std::shared_ptr<utopia::UVector>  Vector;
    typedef std::shared_ptr<libMesh::DistributedMesh> Mesh_ptr;

public:
    Laplace_assembler(libMesh::LibMeshInit &init, const std::string mesh_path, bool bc_term_ = true, double diff_coeff_ = 1.);
    Laplace_assembler(Mesh_ptr mesh_, bool bc_term_ = true, double diff_coeff_ = 1.);

    virtual ~Laplace_assembler(){}

    void assemble();

    inline utopia::SizeType get_size() const{ return size_space;}
    inline std::shared_ptr<utopia::LibMeshFunctionSpace> get_fem_space() const{ return V;}

    inline void set_diff_coeff(const std::string path){

        diff_scalar_flag = false;
        diff_tensor_flag = false;

        auto &dof_map = V->dof_map();

        utopia::UIndexArray ghost_nodes;
        utopia::convert(dof_map.get_send_list(), ghost_nodes);

        diff_coeff = std::make_shared<utopia::UVector>(utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes));
        
        std::ifstream myfile;        
        std::cout << "Reading file" << path << " ..." << std::endl;
        myfile.open(path);

        double d;
        int counter = 0;
        auto range_ = range(*diff_coeff);
        if (myfile.is_open())
        {
            {
                utopia::Write<utopia::UVector> w(*diff_coeff);

                while ( myfile >> d )
                {
                    if (counter == size_space)
                    {
                        std::cout << "ERROR: input file too big!"<< '\n'; //TODO check for blanks?
                        std::cout << "counter = " << counter << '\n'; //TODO check for blanks?                        
                    }
                    if(range_.inside(counter))
                    {
                        diff_coeff->set(counter, d);
                    }
                    counter++;
                }
                myfile.close();
            }
        }

        utopia::synchronize(*diff_coeff);        
    }

    inline void set_diff_coeff(const Vector &diff_coeff_){

        diff_scalar_flag = false;
        diff_tensor_flag = false;

        auto &dof_map = V->dof_map();

        utopia::UIndexArray ghost_nodes;
        utopia::convert(dof_map.get_send_list(), ghost_nodes);

        diff_coeff = std::make_shared<utopia::UVector>(utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes));

        diff_coeff = diff_coeff_;
        utopia::synchronize(*diff_coeff);
    }

    inline void set_diff_tensor(const std::string path){
    
        diff_scalar_flag = false;
        diff_tensor_flag = true;

        utopia::UIndexSet ghost_nodes;
        utopia::convert(V->dof_map().get_send_list(), ghost_nodes);

        std::cout << "Reading tensor diffusion from " << path << " ..." << std::endl;
         
        if(!path.empty() && read_tensor_data(path, V->dof_map().n_local_dofs(), dim*dim, *diff_coeff))
        {
            build_tensor_ghosted(V->dof_map().n_local_dofs(), V->dof_map().n_dofs(), dim*dim, ghost_nodes, *diff_coeff);                        
            
        } else {
            std::cout << "ERROR: problem with reading tensor diffusion" << std::endl;
        }
        
        std::cout << "size = " << size(*diff_coeff) << std::endl;
    }

    inline void set_diff_coeff(double diff_coeff_){

        diff_scalar_flag = true;
        diff_tensor_flag = false;

        auto &dof_map = V->dof_map();

        utopia::UIndexArray ghost_nodes;
        utopia::convert(dof_map.get_send_list(), ghost_nodes);

        diff_coeff = std::make_shared<utopia::UVector>(utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes));
        diff_coeff->set(diff_coeff_);
        utopia::synchronize(*diff_coeff);

        diff_coeff_scalar = diff_coeff_;        
    }

    inline void set_forcing_term(double force){ forcing_term   = force;}
    inline void set_jump_diff()               { diff_jump_flag = true ;}  
    inline void set_mass_lumping()            { mass_lumping   = true ;}    

    inline bool   get_diff_scalar_flag()  const{ return diff_scalar_flag ;}    
    inline double get_diff_coeff_scalar() const{ return diff_coeff_scalar;}
    inline Vector get_diff_coeff()        const{ return diff_coeff       ;}

    inline libMesh::System & get_equation_system() const{ return V->equation_system();}
    inline libMesh::EquationSystems & get_equation_systems() const{ return V->equation_systems();}

    libMesh::DofMap & get_dof_map() const { return V->dof_map();}

    // TODO set FEM space paramenters
    // TODO set_bc Method    

private:
    void initialize();

    double forcing_term = 0.;
    bool bc_term;

    double diff_coeff_scalar;
    Vector diff_coeff;        
    bool diff_scalar_flag;
    bool diff_tensor_flag;
    bool diff_jump_flag = false;

    bool mass_lumping = false;

    utopia::SizeType size_space;

    std::shared_ptr<utopia::LibMeshFunctionSpace> V;

    int dim; // mesh dimension
};

#endif



