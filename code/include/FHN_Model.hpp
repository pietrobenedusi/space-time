#ifndef UTOPIA_FHN_MODEL_HPP
#define UTOPIA_FHN_MODEL_HPP

#include "utopia.hpp"
#include "utopia_fe_core.hpp"
#include "utopia_libmesh_NonLinearFEFunction.hpp"

namespace utopia {
    template<class FunctionSpace, class Matrix, class Vector, int Backend = Traits<Vector>::Backend>
    class FHN_Model : public Function<Matrix, Vector> {
    public:
        DEF_UTOPIA_SCALAR(Matrix);
        
        FHN_Model(std::shared_ptr<FunctionSpace> V, const Scalar alpha, const Scalar u_min, const Scalar u_max, const Scalar u_unst ) : 
            V_(V), alpha_(alpha), u_min_(u_min), u_max_(u_max), u_unst_(u_unst){}

        virtual ~FHN_Model() {}
        
        virtual bool value(const Vector &x, Scalar &value) const
        {
            //FIXME
            assert(false && "implement me");
            return -1;
        }
        
        virtual bool gradient(const Vector &x, Vector &result) const
        {
            Vector grad_space;

            auto u = trial(*V_);  
            auto v = test(*V_);

            auto uk     = interpolate(x, u); 
            auto u_min  = coeff(u_min_);
            auto u_max  = coeff(u_max_);
            auto u_unst = coeff(u_unst_);

            auto l = (uk - u_min) * (uk - u_max) * (uk - u_unst);
            auto f_form = alpha_ * inner(l, v) * dX;

            utopia::assemble(f_form, result);

            return true;
        }
        
        virtual bool hessian(const Vector &x, Matrix &H) const
        {
            // TODO 
            auto u = trial(*V_);
            auto v = test(*V_);
            
            auto uk     = interpolate(x, u);
            auto u_min  = coeff(u_min_);
            auto u_max  = coeff(u_max_);
            auto u_unst = coeff(u_unst_);
            
            auto l = ( (uk - u_min) * (uk - u_max) + (uk - u_min) * (uk - u_unst) + (uk - u_max) * (uk - u_unst) );
            auto j_form = - alpha_ * inner(l * u, v) * dX;
            
            utopia::assemble(j_form, H);

            return true;
        }
                
    private:
        
        std::shared_ptr<FunctionSpace> V_;
        
        Scalar alpha_, u_min_, u_max_, u_unst_;        
    };
}
#endif //UTOPIA_FHN_MODEL_HPP
