#ifndef SPACE_TIME_FHN_MODEL_HPP
#define SPACE_TIME_FHN_MODEL_HPP

#include "utopia.hpp"
#include "utopia_fe_core.hpp" // TODO, need this?
#include "utopia_libmesh_NonLinearFEFunction.hpp"

#include "Space_time_assembler.hpp"

namespace utopia {
    template<class FunctionSpace, class Matrix, class Vector, int Backend = Traits<Vector>::Backend>
    class Space_time_FHN_Model : public LeastSquaresFunction<Matrix, Vector>,   
                                 public Function<Matrix, Vector> {
    public:
        DEF_UTOPIA_SCALAR(Matrix);
        
        Space_time_FHN_Model(const std::shared_ptr<Space_time_assembler> Space_time_op_ptr, std::shared_ptr<FunctionSpace> V, 
                             const Scalar alpha, const Scalar u_min, const Scalar u_max, const Scalar u_unst, const bool implicit_flag = true) :  
                        Space_time_op_ptr_(Space_time_op_ptr), alpha_(alpha), u_min_(u_min), u_max_(u_max), u_unst_(u_unst), 
                        implicit_flag_(implicit_flag){

                if (!Space_time_op_ptr_->get_assembled_flag() && mpi_world_rank() == 0) std::cerr << "ERRROR : Space time op in Space_time_FHN_Model has not been assembled." << std::endl;
                                    
                Space_time_system_ = Space_time_op_ptr_->get_space_time_system();
                Space_time_rhs_    = Space_time_op_ptr_->get_space_time_rhs();                                    
                Space_time_mass_   = Space_time_op_ptr_->get_space_time_mass(implicit_flag_);                
            }

        virtual ~Space_time_FHN_Model() {}
        
        virtual bool value(const Vector &x, Scalar &value) const override
        {
            //FIXME
            Vector res; 
            residual(x, res);

            value = 0.5 * pow(utopia::norm2(res),2);
            return 0;
        }

        bool jacobian(const Vector &x, Matrix &jacobian) const override // metto solo una moltiplicazione matriciale con diag?
        {
            auto size_loc = local_size(x).get(0);
    
            Vector u_min  = local_values(size_loc, u_min_);
            Vector u_max  = local_values(size_loc, u_max_);
            Vector u_unst = local_values(size_loc, u_unst_);                        

            Vector I_ion_deriv_vec =  alpha_ * (e_mul(x - u_min, x - u_max) + e_mul(x - u_min, x - u_unst) + e_mul(x - u_unst, x - u_max));                            
            Matrix I_ion_deriv = (*Space_time_mass_) * diag(I_ion_deriv_vec);
                                          
            jacobian = (*Space_time_system_) - I_ion_deriv;     
            
            return true;              
        }

        bool residual(const Vector &point, Vector &residual) const override 
        {

            auto size_loc = local_size(point).get(0);
        
            Vector u_min  = local_values(size_loc, u_min_);
            Vector u_max  = local_values(size_loc, u_max_);
            Vector u_unst = local_values(size_loc, u_unst_);

            Vector I_ion = alpha_ * e_mul(e_mul(point - u_min, point - u_max), (point - u_unst));    
           
            residual = (*Space_time_system_)*point - (*Space_time_mass_)*I_ion - (*Space_time_rhs_);
                        
            return true;            
        }
        
        virtual bool gradient(const Vector &x, Vector &result) const override
        {                                                    
            residual(x, result);                                          
            return true;
        }
        
        virtual bool hessian(const Vector &x, Matrix &H) const override
        {
            jacobian(x, H);        

            return true;
        }
               
    private:        

        std::shared_ptr<Space_time_assembler> Space_time_op_ptr_; // this must be a Laplacian in space
        
        std::shared_ptr<Matrix> Space_time_system_;
        std::shared_ptr<Matrix> Space_time_mass_;
        std::shared_ptr<Vector> Space_time_rhs_;
        std::shared_ptr<Matrix> time_mass_;  

        unsigned int time_steps_;
        int time_slab_size_;  

        Scalar alpha_, u_min_, u_max_, u_unst_;        

        bool implicit_flag_;
    };
}
#endif 
