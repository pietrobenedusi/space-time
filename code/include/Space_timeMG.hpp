#ifndef Space_time_MG_hpp
#define Space_time_MG_hpp

#include "Space_time_assembler.hpp"
#include <vector>

class Space_timeMG{

	typedef std::shared_ptr<libMesh::DistributedMesh> Mesh_ptr;
	typedef std::shared_ptr<Space_time_assembler>	Space_time_assembler_ptr;

public:
	// typedef utopia::Multigrid<utopia::USparseMatrix, utopia::UVector, utopia::PETSC_EXPERIMENTAL> Multigrid;
	typedef utopia::Multigrid<utopia::USparseMatrix, utopia::UVector> Multigrid;

	Space_timeMG(	libMesh::Parallel::Communicator &comm, const std::shared_ptr<Space_time_assembler> space_time_op_, const unsigned int number_of_levels_);
	// Space_timeMG(const libMesh::LibMeshInit &init_, const std::shared_ptr<Space_time_assembler> space_time_op_, const unsigned int number_of_levels_);

	void set_spatial_meshes();
	void set_spatial_meshes(const std::vector<Mesh_ptr> &spatial_meshes_);

	template<class SpaceAssemblerT>
	void set_spatial_ops(bool use_galerkin_ass = true)
	{
		spatial_ops.reserve(number_of_levels); // here the fine operator is included

		spatial_ops.push_back(space_time_op->get_space_op());

		for (unsigned int i = 0; i < number_of_levels - 1; ++i)
		{
			auto ass_ptr = std::make_shared<SpaceAssemblerT>(spatial_meshes[i], false);

			// set diffusion coeff (needed for coarsening ops)
			if (!use_galerkin_ass)
			{
				if (ass_ptr->get_diff_scalar_flag()) // in case it is a scalar
				{
					ass_ptr->set_diff_coeff(space_time_op->get_space_op()->get_diff_coeff_scalar());
				}
				else // in case diffusion is a vector
				{
					// transfer diff
					utopia::USparseMatrix transfer_space_fn_cr, transfer_space_fn_cr_tmp;
					coupling(*spatial_ops[i], *ass_ptr, transfer_space_fn_cr_tmp, false);
        			transfer_space_fn_cr = transpose(transfer_space_fn_cr_tmp);

        			auto diff_vec = space_time_op->get_space_op()->get_diff_coeff();

        			auto &dof_map = ass_ptr->get_dof_map();
        			utopia::UIndexArray ghost_nodes;
        			utopia::convert(dof_map.get_send_list(), ghost_nodes);

			        utopia::UVector diff_vec_coarse = utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes);
			        diff_vec_coarse = transfer_space_fn_cr * (*diff_vec);

			        auto diff_vec_coarse_ptr = std::make_shared<utopia::UVector>(diff_vec_coarse);

        			ass_ptr->set_diff_coeff(diff_vec_coarse_ptr);
				}
			}

			spatial_ops.push_back(ass_ptr); //  bc_term = false, no BC on coarser meshes
		}
	}

	// template<class SpaceAssemblerT>
	// void coupling_time(const int time_steps_fine, const int time_steps_coarse, utopia::USparseMatrix &I){

	//     auto mesh_cr = std::make_shared<libMesh::DistributedMesh>(init->comm());
	//     auto mesh_fn = std::make_shared<libMesh::DistributedMesh>(init->comm());

	//     libMesh::MeshTools::Generation::build_line(*mesh_cr, time_steps_fine, 0, 1, libMesh::EDGE2);
	// 	libMesh::MeshTools::Generation::build_line(*mesh_fn, time_steps_coarse, 0, 1, libMesh::EDGE2);

	//     auto & cr_space = *std::make_shared<SpaceAssemblerT>(mesh_cr, false);
	//     auto & fn_space = *std::make_shared<SpaceAssemblerT>(mesh_fn, false);

	//     coupling(fn_space, cr_space, I);
	// }


	template<class SpaceAssemblerT>
	void init_mg(bool use_galerkin_ass = true)
	{
		using namespace utopia;

		init_flag_ = true;

		// set smoother and direct solver for MG
		auto smoother = std::make_shared<utopia::GMRES<utopia::USparseMatrix, utopia::UVector>>();
		// auto smoother = std::make_shared<utopia::SOR<utopia::USparseMatrix, utopia::UVector>>();
		smoother->pc_type("bjacobi");		
		// smoother->verbose(true); // make clean
	
		// auto direct_solver = std::make_shared<utopia::Factorization<utopia::USparseMatrix, utopia::UVector>>();
		auto direct_solver = std::make_shared<utopia::BiCGStab<utopia::USparseMatrix, utopia::UVector>>();
		// auto direct_solver = std::make_shared<utopia::GMRES<utopia::USparseMatrix, utopia::UVector>>();		
		// direct_solver->verbose(true); // make clean
		// direct_solver->rtol(1e-5);
		// direct_solver->atol(1e-5);

		multigrid = std::make_shared<Space_timeMG::Multigrid>(smoother, direct_solver);

		multigrid->fix_semidefinite_operators(true);		
		
		multigrid->pre_smoothing_steps(smoothing_steps_);
		multigrid->post_smoothing_steps(smoothing_steps_);

		if (spatial_meshes.empty()) set_spatial_meshes(); 		
		set_spatial_ops<SpaceAssemblerT>(use_galerkin_ass);
		set_interpolation_operators();

		multigrid->set_transfer_operators(interpolation_operators);

		if (!use_galerkin_ass)
		{
			std::cout << "Galerkin assembled NOT used" << std::endl;

			// multigrid->set_perform_galerkin_assembly(false);
			// set_coarse_operators();
			// multigrid->set_linear_operators(coarse_operators);
		}	
	}

	// template<class SpaceAssemblerT> void init_mg() // TODO check tols !!!
	// {
	// 	init_flag_ = true;

	// 	// create MG solver
	// 	PetscErrorCode ierr;

	// 	auto st_system = space_time_op->get_space_time_system();

	// 	ierr = KSPCreate(PETSC_COMM_WORLD, &multigrid);CHKERRV(ierr);
 //    	ierr = KSPSetOperators(multigrid, raw_type(*st_system), raw_type(*st_system));CHKERRV(ierr);
 //    	ierr = KSPSetType(multigrid,KSPRICHARDSON);CHKERRV(ierr);
 //    	ierr = KSPSetFromOptions(multigrid);CHKERRV(ierr);
 //    	// ierr = KSPSetUp(multigrid);CHKERRV(ierr);

 //        PC pcmg;
 //        ierr = KSPGetPC(multigrid, &pcmg);
 //        ierr = PCSetType(pcmg, PCMG);
 //        ierr = PCMGSetLevels(pcmg,number_of_levels, NULL);CHKERRV(ierr);
 //        ierr = PCMGSetType(pcmg,PC_MG_KASKADE);CHKERRV(ierr);
 //        // ierr = PCMGSetGalerkin(pcmg,PC_MG_GALERKIN_BOTH);CHKERRV(ierr);

 //        // construct transfer operators
	// 	set_spatial_meshes();
	// 	set_spatial_ops<SpaceAssemblerT>(true);
	// 	set_interpolation_operators();

	// 	// set smoothers and transfers
 //        for (int i = 0; i < number_of_levels; i++)
 //        {
 //            KSP smoother;
 //            ierr = PCMGGetSmoother(pcmg, i, &smoother);CHKERRV(ierr);
 //            ierr = KSPSetType(smoother, KSPGMRES);CHKERRV(ierr);
 //            ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, smoothing_steps_);CHKERRV(ierr);
 //            ierr = KSPSetNormType(smoother, KSP_NORM_NONE);CHKERRV(ierr);

 //            // set interpolators
 //            if (i > 0) ierr = PCMGSetInterpolation(pcmg, i, raw_type(*get_transfer_op(i-1)));CHKERRV(ierr);
 //        }

 //        ierr = KSPSetUp(multigrid);CHKERRV(ierr);
	// }

	// To avoid galerkin assembling // DIFFUSION for BOTH cases! FIXME!
	void set_coarse_operators();

	inline void set_coarsening_space( double coarsening_space_) {coarsening_space = coarsening_space_;}
	inline void set_coarsening_time(  int coarsening_time_ ) {coarsening_time  = coarsening_time_; }

	// coupling methods
	void coupling(Space_assembler &fine_op, Space_assembler &coarse_op, utopia::USparseMatrix &interp_op, const bool transfer_residual = true);
	PetscErrorCode coupling(const int time_steps_fine, const int time_steps_coarse, Mat &I);
	// void coupling_time(const int time_steps_fine, const int time_steps_coarse, utopia::USparseMatrix &I);
	PetscErrorCode set_interpolation_operators();

	// interpolation or L2 projection for the transfer
	inline void set_interpolation_flag(const bool use_interpolation){use_interpolation_ = use_interpolation;}

	// smoothing steps
	inline void set_smoothing_steps(const int smoothing_steps){smoothing_steps_ = smoothing_steps;}

	// getter
	inline const Space_time_assembler_ptr get_space_time_op() const{ return space_time_op;}

	// solve
	void solve(utopia::UVector &solution, bool verbose_flag = true);

	// multigrid object with its solvers
	std::shared_ptr<Multigrid>    multigrid;
	// KSP multigrid;

	// print space-time disc. ratio
    void print_mu(double diffusivity = 1.) const;

    // getters
    inline Mesh_ptr get_mesh(const unsigned int i){

    	if (i >= spatial_meshes.size()) throw std::out_of_range ("out_of_range");
    	return spatial_meshes[i];
    }

    inline std::shared_ptr<Space_assembler> get_spatial_op(const int unsigned i){

    	if (i >= spatial_ops.size()) throw std::out_of_range ("out_of_range");
    	return spatial_ops[i];
    }

    inline std::shared_ptr<utopia::USparseMatrix> get_transfer_op(const int unsigned i){

    	if (i >= interpolation_operators.size()) throw std::out_of_range ("out_of_range");
    	return interpolation_operators[i];
    }

private:

	// const libMesh::LibMeshInit *init;
	libMesh::Parallel::Communicator *comm;

	double coarsening_space = 2;
	unsigned int coarsening_time = 4;

	bool use_interpolation_ = true;
	bool init_flag_ = false;

	Space_time_assembler_ptr space_time_op;

	// Finer mesh has index 0 and the coarser one is the last one (Base fine mesh is not included here)
	std::vector< Mesh_ptr > spatial_meshes;
	std::vector< std::shared_ptr<Space_assembler> > spatial_ops; // here the fine operator is included! // TODO?

	const unsigned int number_of_levels;
	int smoothing_steps_ = 3;

	std::vector<std::shared_ptr<utopia::USparseMatrix> > interpolation_operators; // here 0 is the coarser
	std::vector<std::shared_ptr<const utopia::USparseMatrix> >  coarse_operators;
};

#endif
