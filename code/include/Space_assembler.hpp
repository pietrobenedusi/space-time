#ifndef Space_assembler_hpp
#define Space_assembler_hpp

#include "utopia.hpp"
#include "utopia_fe_core.hpp"
#include "utopia_LibMeshBackend.hpp"
#include "libmesh/parallel_mesh.h"
#include "utopia_libmesh_NonLinearFEFunction.hpp"

class Space_assembler{

    typedef std::shared_ptr<utopia::USparseMatrix> Matrix;
    typedef std::shared_ptr<utopia::UVector>  Vector;
    typedef std::shared_ptr<libMesh::DistributedMesh> Mesh_ptr;

public:

    virtual void assemble() = 0;

    // getters
    inline Matrix get_mass()  const{ return mass;  }
    inline Matrix get_stiff() const{ return stiff; }
    inline Vector get_rhs()    const{ return rhs;  }

    virtual libMesh::System & get_equation_system() const = 0;
    virtual libMesh::EquationSystems & get_equation_systems() const = 0;
    //virtual std::shared_ptr<utopia::LibMeshFunctionSpace> get_fem_space() const = 0;

    virtual utopia::SizeType get_size() const = 0;

    virtual libMesh::DofMap & get_dof_map() const = 0;

    inline Mesh_ptr get_mesh() const { return mesh; }

    // TODO this hould not be here
    virtual double get_diff_coeff_scalar() const = 0;
    virtual Vector get_diff_coeff()        const = 0;

protected:

    Mesh_ptr mesh;

    Matrix  mass;
    Matrix stiff;
    Vector   rhs;

    // TODO this hould not be here
    double diff_coeff_scalar;
    Vector diff_coeff;
};

#endif



