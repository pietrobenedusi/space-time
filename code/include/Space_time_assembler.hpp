#ifndef Space_time_assembler_hpp
#define Space_time_assembler_hpp

#include "kron.hpp"
#include "Space_assembler.hpp"
#include "Time_slab_assembler.hpp"
#include "libmesh/parallel_mesh.h"
#include <iostream>

class Space_time_assembler{

    typedef std::shared_ptr<utopia::USparseMatrix> Matrix;
    typedef std::shared_ptr<utopia::UVector>  Vector;
    typedef std::shared_ptr<utopia::LibMeshFunctionSpace>  FE_space;

public:
    Space_time_assembler(const std::shared_ptr<Space_assembler> space_ops_, const std::shared_ptr<Time_slab_assembler> time_ops_, const int time_steps_ = 10) :
        space_ops(space_ops_),
        time_ops(time_ops_),
        time_steps(time_steps_),
        compute_prec_(false){

        init_sizes();
    }

    template<class SpaceAssemblerT>
    void init_space_time_assembler(libMesh::LibMeshInit &init, const std::string mesh_path, const double time_step = 1, const int time_steps_ = 10){

        space_ops = std::make_shared<SpaceAssemblerT>(init, mesh_path);
        time_ops = std::make_shared<Time_slab_assembler>(time_step);

        time_steps = time_steps_;

        space_ops->assemble();
        time_ops->assemble();

        init_sizes();
    }

    // methods
    PetscErrorCode assemble();
    PetscErrorCode assemble_rhs();
    void           assemble_rhs(utopia::UVector &rhs_space, utopia::UVector &st_rhs);

    // set and get initial condition
    void set_time_bc(const int boundary_tag, const double u_in, const double u_out, const FE_space V); //TODO improve this, no FE space
    void set_time_bc(const double time_bc_, bool bc = true);  // bc true applies space BC to the initial condition

    inline void set_time_bc(const Vector time_bc_, const bool bc = false){

        flag_time_bc = true;
        time_bc = time_bc_;

        if (bc) // apply space BC to the intial condition
        {
            apply_zero_boundary_conditions( space_ops->get_dof_map(), *time_bc);
            // TODO in general it is not zero...
        }
    }

    inline void suppress_flag_time_bc_warnings(){ flag_time_bc = true; }

    inline void set_compute_prec(const bool val){ compute_prec_ = val; }

    inline void set_space_time_system(const Matrix space_time_system_) {space_time_system = space_time_system_;}
    inline void set_space_time_system(const utopia::USparseMatrix space_time_system_) {space_time_system = std::make_shared<utopia::USparseMatrix>(space_time_system_);}
    inline void set_space_time_rhs(const utopia::UVector space_time_rhs_) {space_time_rhs = std::make_shared<utopia::UVector>(space_time_rhs_);}

    // getters
    inline const Matrix &get_space_time_system() const{ return space_time_system;}

    inline const Matrix &get_space_time_system_prec() const{
        if(space_time_system_prec) {
            return space_time_system_prec;
        } else {
            std::cerr << "[Warning] does not have preconditioner system" << std::endl;
            return space_time_system;
        }
    }
    inline const Vector &get_space_time_rhs()    const{ return space_time_rhs;   }
    inline const Vector &get_time_bc()           const{ return time_bc;          }

    inline unsigned int get_time_steps() const{ return time_steps;}
    inline utopia::SizeType     get_size_A()     const{ return size_A;    }
    inline utopia::SizeType     get_size_ST()    const{ return size_ST;   }

    inline std::shared_ptr<Space_assembler>    get_space_op() const{ return space_ops;}
    inline std::shared_ptr<Time_slab_assembler> get_time_op() const{ return time_ops; }

    PetscErrorCode set_space_time_mass(bool implicit_flag = true);    
    PetscErrorCode set_time_slabs_mass();

    inline Matrix get_space_time_mass(bool implicit_flag = true){

        if(!Mass) set_space_time_mass(implicit_flag);
    
        return Mass;
    }

    inline Matrix get_time_slabs_mass(){

        if(!time_slabs_mass) set_time_slabs_mass();

        return time_slabs_mass;
    }

    inline bool get_assembled_flag() const{ return assembled_flag_;}

    // print space-time disc. ratio
    void print_mu(double diffusivity = 1.) const;

private:
    Matrix A;
    Matrix A_prec;    // symbol based A, can be used as preconditioner
    Matrix B;
    Matrix Mass;            // I_t kron M_t kron M_s
    Matrix time_slabs_mass; // I_t kron M_t kron I_s
    Matrix space_time_system;
    Matrix space_time_system_prec;
    Vector space_time_rhs;

    // bc
    Vector time_bc;
    bool flag_time_bc = false;

    std::shared_ptr<Space_assembler>     space_ops;
    std::shared_ptr<Time_slab_assembler> time_ops;

    // sizes
    unsigned int time_steps;
    utopia::SizeType size_A;
    utopia::SizeType size_ST; // size of space_time_system = (size_A * time_steps)
    void init_sizes();

    // assembling routines
    PetscErrorCode assemble_slab_blocks();
    PetscErrorCode assemble_preconditioner();

    // flags
    bool compute_prec_ = false;
    bool assembled_flag_ = false;
};

#endif

