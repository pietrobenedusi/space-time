#ifndef Time_trap_assembler_hpp
#define Time_trap_assembler_hpp

#include "Time_slab_assembler.hpp"

class Time_trapezoidal_assembler : public Time_slab_assembler{

public:

    Time_trapezoidal_assembler(const double time_step_){

        time_step = time_step_;
        coupling_need_stiffness_ = true;
        size_time_slab = 1;
    }

    void assemble();
};

#endif 



