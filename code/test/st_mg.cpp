#include "Space_timeMG.hpp"
#include "Space_time_FHN_Model.hpp"
#include "Time_trapezoidal_assembler.hpp"
#include "Laplace_assembler.hpp"
#include "Eikonal_assembler.hpp"
#include "Utilities.hpp"

using namespace libMesh;
using namespace utopia;
using std::make_shared;
int main(const int argc, char *argv[])
{
    libMesh::LibMeshInit init(argc, argv);
    // input check
    bool pp; // for preprocessing

    // montecarlo stuff
    bool mc_flag = 0; // for montecarlo
    unsigned int mc_level;

    const bool output = true;

    // ------------- problem parameters ---------------- //

    // MG levels, initialized later from user input
    unsigned int levels = 1;
    // FHN paramters
    double u_min = -85., u_max = 30., u_unst = -57.6, u_source = 25.;
    // double u_min = 0., u_max = 115., u_unst = 28., u_source = 110.;
    double alpha = -1.4e-3;
    double chi   = 400.;
    double diffusivity = 1.33; // TODO set this from file and check

    // for gaussian initial condition
    double sigma = 1.;
    double x0 = -1.;
    double y0 =  1.;
    double z0 =  1.;

    // time parameters
    double dt = 0.01;
    unsigned int time_steps;

    // reading input
    if (argc < 2)
    {
        std::cerr << "ERROR: correct usage: ./st_mg -pp level_num" << std::endl;
        std::cerr << "                      ./st_mg -solve time_steps level_num sample_filename" << std::endl;
        return 1;
    }
    if (strcmp(argv[1], "-pp") == 0)
    {
        pp = true;
        time_steps = 1;
        if (argc == 2)
        {
            levels = 0;
        }
        else if (argc == 3)
        {
            levels = atoi(argv[2]);   // get the number of levels
        }
        else{
            std::cerr << "ERROR: correct usage: ./st_mg -pp level_num" << std::endl;
            return 1;
        }
    }
    else if (strcmp(argv[1], "-solve") == 0){
        pp = false;
        time_steps = atoi(argv[2]);
        levels = atoi(argv[3]);
        if ( argc == 6)
        {
            mc_flag = true;
            mc_level = atoi(argv[5]);
            if (mc_level < 1 || mc_level > levels )
            {
                std::cerr << "ERROR: MC level mc_l should satisfy: 1 <= mc_l <= level  " << std::endl;
                return 1;
            }
        }
        if (argc != 5 && argc != 6)
        {
            std::cerr << "ERROR: some argument is missing" << std::endl;
            std::cerr << "USAGE: ./st_mg -solve time_steps level_num sample_filename" << std::endl;
            return 1;
        }
    }
    else {
        std::cerr << "ERROR: second argument can be -pp for preprocessing or -solve for solving." << std::endl;
        return 1;
    }

    // mesh
    auto mesh = make_shared<libMesh::DistributedMesh>(init.comm());
    // mesh->read("../../data/rect2.1.e");
    // mesh->read("../../data/heart2.e");
    mesh->read("../../data/cube_test_tet01.e");
    //mesh->read("../../data/sphere-tet-6075.e");
    int boundary_tag = 5; // TODO set this from outside, now hardcoded

    // Eikonal parameters
    double c0 = 1.;
    double tau = 2.; //70.;

    Laplace_assembler s_ass(mesh, false);

    if (pp)
    {
        save_dofs_coord(s_ass, mesh);
        // create mass matrix
        s_ass.assemble();
        save_mat(raw_type(*s_ass.get_mass()),"log_hessian.m","H");
        if (levels == 0) return 0;   // if there are no levels specified exit
    }

    auto &dof_map = s_ass.get_dof_map();
    utopia::UIndexArray ghost_nodes;
    convert(dof_map.get_send_list(), ghost_nodes);

    auto diff_vec = std::make_shared<utopia::UVector>(utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes));

    if (!pp) // get diffusion from file
    {
        //std::string line;
        std::ifstream myfile;
        std::cout << "Reading file " << argv[4] << std::endl;
        myfile.open(argv[4]);

        double d;
        int counter = 0;
        auto range_ = range(*diff_vec);
        if (myfile.is_open())
        {
            {
                Write<UVector> w(*diff_vec);
                while ( myfile >> d )
                {
                    if (counter == s_ass.get_size())
                    {
                        std::cout << "ERROR: input file too big!"<< '\n'; //TODO check for blanks?
                        std::cout << "counter = " << counter << '\n'; //TODO check for blanks?
                        return 1;
                    }
                    if(range_.inside(counter))
                    {
                        diff_vec->set(counter, d);
                    }
                    counter++;
                }
                myfile.close();
            }
        }
        else{
            std::cout << "Unable to open file"<< '\n';
            return 1;
        }
        if (counter != s_ass.get_size())
        {
            std::cout << "ERROR: input file has worng size!"<< '\n';
            std::cout << "Counter = " << counter << '\n';
            std::cout << "DoFs = " << s_ass.get_size() << '\n';
            return 1;
        }

        // ============================================== //
        *diff_vec *= (1./chi);
        utopia::synchronize(*diff_vec);

        // If -solve continue
        s_ass.set_diff_coeff(diff_vec);
        //s_ass.set_diff_coeff(diffusivity/chi);

        s_ass.assemble(); // TODO is this necessary for mc? not really

        std::cout << "--------- space operator assembled -----------" << std::endl;
    }

    auto s_ass_ptr = make_shared<Laplace_assembler>(s_ass);

    //time
    auto t_ass_ptr = make_shared<Time_slab_assembler>(dt);
    // auto t_ass_ptr = make_shared<Time_trapezoidal_assembler>(dt);
    if (!pp && !mc_flag)
    {
        t_ass_ptr->assemble();
        std::cout << "--------- time operator assembled -----------" << std::endl;
    }

    // space-time
    auto st_op_ptr = make_shared<Space_time_assembler>(s_ass_ptr, t_ass_ptr, time_steps);
    // copy of unassembled pace-time op for Eikonal (always solved on fine)
    auto st_op_fn = *st_op_ptr;
    if (!pp) // this is not necassary for pp
    {
        st_op_ptr->print_mu(diffusivity/chi); // TODO update this

        if (!mc_flag)
        {
            // set initial condition
            auto inital_forcing_ptr = make_shared<UVector>();
            set_gaussian_forcing(s_ass, *inital_forcing_ptr, u_source - u_min, u_min, sigma, x0, y0, z0);
            st_op_ptr->set_time_bc(inital_forcing_ptr);

            // st_op_ptr->set_time_bc(boundary_tag, u_source, u_min, s_ass.get_fem_space());

            // assemble
            st_op_ptr->assemble();
            std::cout << "--------- space-time operator assembled -----------" << std::endl;
        }
    }

    USparseMatrix transfer_space_fn_cr;
    USparseMatrix transfer_space_time_cr_fn, transfer_space_time_fn_cr;
    std::shared_ptr<UVector> time_bc;
    std::shared_ptr<libMesh::DistributedMesh> mesh_mc;

    if (mc_flag) // Montecarlo stuff
    {
        std::cout << "--------- Montacarlo starting -----------" << std::endl;
        Space_timeMG st_mg_aux(init, st_op_ptr, mc_level);
        st_mg_aux.set_coarsening_space(2);
        st_mg_aux.set_spatial_meshes();
        st_mg_aux.set_spatial_ops<Laplace_assembler>();
        mesh_mc = st_mg_aux.get_mesh(mc_level - 1);

        int time_steps_cr = 1 + (time_steps - 1)/pow(2,mc_level);

        double dt_mc = dt * pow(2,mc_level);

        USparseMatrix transfer_space, transfer_time, transfer_space_fn_cr_tmp;

        // build spatial transfer fn to cr
        st_mg_aux.coupling(*st_mg_aux.get_spatial_op(0), *st_mg_aux.get_spatial_op(mc_level), transfer_space_fn_cr_tmp, false);
        transfer_space_fn_cr = transpose(transfer_space_fn_cr_tmp);

        // build spatial transfer cr to fn
        st_mg_aux.set_interpolation_flag(true); // just for cf to fn
        st_mg_aux.coupling(*st_mg_aux.get_spatial_op(0), *st_mg_aux.get_spatial_op(mc_level), transfer_space);

        for (int i = 0; i < mc_level; ++i) // create temrporal restriction and assemble space-time res
        {
            USparseMatrix transfer_time_tmp, transfer_space_time_slab;

            int time_steps_interm_fn = 1 + (time_steps - 1)/pow(2,i);
            int time_steps_interm_cr = 1 + (time_steps - 1)/pow(2,i+1);

            st_mg_aux.coupling(time_steps_interm_fn, time_steps_interm_cr, raw_type(transfer_time_tmp));

            if (i == 0)
            {
                transfer_time = transfer_time_tmp;
            }
            else
            {
                transfer_time *= transfer_time_tmp;
            }
            if (i == mc_level - 1)
            {
                auto time_slab_size = t_ass_ptr->get_size();

                USparseMatrix I_time_slab = identity(time_slab_size,time_slab_size);

                PetscErrorCode ierr;

                // CR to FN
                ierr = kron(raw_type(transfer_space), raw_type(I_time_slab), raw_type(transfer_space_time_slab));CHKERRQ(ierr);
                ierr = kron(raw_type(transfer_time), raw_type(transfer_space_time_slab), raw_type(transfer_space_time_cr_fn));CHKERRQ(ierr);

                //FN to CR
                transfer_time = (2./3.) * transpose(transfer_time);

                // USparseMatrix blbla = transpose(transfer_space_fn_cr);
                ierr = kron(raw_type(transfer_space_fn_cr), raw_type(I_time_slab), raw_type(transfer_space_time_slab));CHKERRQ(ierr);
                ierr = kron(raw_type(transfer_time), raw_type(transfer_space_time_slab), raw_type(transfer_space_time_fn_cr));CHKERRQ(ierr);
            }
        }

        // for fn to cr l2 proj is used
        std::cout << "--------- Transfer diffusion -----------" << std::endl;

        // transfer diffusion
        auto &dof_map = st_mg_aux.get_spatial_op(mc_level)->get_dof_map();
        utopia::UIndexArray ghost_nodes;
        convert(dof_map.get_send_list(), ghost_nodes);

        UVector diff_vec_mc = utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes);
        diff_vec_mc = transfer_space_fn_cr * (*diff_vec);

        auto diff_vec_mc_ptr = std::make_shared<utopia::UVector>(diff_vec_mc);

        std::cout << "--------- Create coarse problem -----------" << std::endl;

        // coarse space
        auto s_ass_cr_ptr = make_shared<Laplace_assembler>(mesh_mc, false); // TODO this should be the same of MLMC?
        s_ass_cr_ptr->set_diff_coeff(diff_vec_mc_ptr);
        s_ass_cr_ptr->assemble();

        // coarse time
        auto t_ass_cr_ptr = make_shared<Time_slab_assembler>(dt_mc);

        // auto t_ass_cr_ptr = make_shared<Time_trapezoidal_assembler>(dt_mc);
        t_ass_cr_ptr->assemble();
        auto time_bc_tmp = st_op_ptr->get_time_bc();

        // coarse space-time
        st_op_ptr = make_shared<Space_time_assembler>(s_ass_cr_ptr, t_ass_cr_ptr, time_steps_cr);

        // coarse time bc
        auto inital_forcing_ptr = make_shared<UVector>();
        set_gaussian_forcing(*s_ass_cr_ptr, *inital_forcing_ptr, u_source - u_min, u_min, sigma, x0, y0, z0);
        st_op_ptr->set_time_bc(inital_forcing_ptr);

        // assemble
        st_op_ptr->assemble();
        // update levels
        levels -= mc_level;
        // save_space_solution(*s_ass_cr_ptr, diff_vec_mc,"../output/diff_cr1.e");
        // return 0;

        // for newton
        s_ass = *s_ass_cr_ptr;
    }

    // Space_timeMG st_mg(init, st_op_ptr, levels);

    // if (levels > 0)
    // {
    //     // st_mg.set_interpolation_flag(true);
    //     // st_mg.set_coarsening_space(2);

    //     std::cout << "--------- space-time MG created -----------" << std::endl;
    // }

    // auto linear_solver = std::make_shared<BiCGStab<USparseMatrix, UVector>>();
    auto linear_solver = std::make_shared<Factorization<USparseMatrix, UVector>>();

    if (levels > 0)
    {
        // auto smoother = std::make_shared<GMRES<USparseMatrix, UVector>>();
        // smoother->sweeps(3);
        // st_mg.multigrid = std::make_shared<Space_timeMG::Multigrid>(smoother, linear_solver);

        // st_mg.multigrid->max_it(100);
        // st_mg.multigrid->mg_type(1);
        // // st_mg.multigrid->rtol(1e-8);
        // // st_mg.multigrid->atol(1e-14);
        // // st_mg.multigrid->stol(1e-14);

        // st_mg.init_mg<Laplace_assembler>();
        // std::cout << "--------- space-time MG was init -----------" << std::endl;

        // st_op_ptr->print_mu(diffusivity/chi);
        // st_mg.print_mu(diffusivity/chi);
    }

    // initial guess
    UVector x_0;

    {
        std::cout << "--------- Solve Eikonal -----------" << std::endl;
        if (!mc_flag)
        {
            auto s_ST = local_size(*(st_op_ptr->get_space_time_system())); // this is present just for assembled? check this
            x_0  = local_zeros(s_ST.get(0));
        }
        else{
            x_0  = zeros(st_op_fn.get_size_ST());
        }

        Eikonal_assembler eikonal(mesh, boundary_tag, tau, c0);
        eikonal.solve();
        eikonal_to_space_time(st_op_fn, *eikonal.get_solution(), x_0, u_min, u_max);
    }
    if(mc_flag)
    {
        // transfer eikonal solution
        x_0  = transfer_space_time_fn_cr * x_0;
    }

    // output file
    SpaceTimeIO io(*mesh_mc, "../output/monodomain_cr.e");
    // SpaceTimeIO io(*mesh, "../output/monodomain.e");

    // SpaceTimeIO io(*mesh_mc, "../output/eikonal.e");
    // io.write_time_block(*st_op_ptr, x_0);
    // return 0;

    // solve
    Space_time_FHN_Model<LibMeshFunctionSpace, USparseMatrix, UVector> st_monodomain(st_op_ptr, s_ass.get_fem_space(),
                                                                                  alpha, u_min, u_max, u_unst);

    if (levels > 0)
    {
        // st_mg.multigrid->verbose(true);
        // st_mg.multigrid->describe(std::cout);

        // Newton<USparseMatrix, UVector> nlsolver(st_mg.multigrid);

        auto gmres_ = std::make_shared<GMRES<USparseMatrix, UVector>>();

        // gmres_->max_it(1000);
        Newton<USparseMatrix, UVector> nlsolver(gmres_);

        // auto strategy = std::make_shared<utopia::Backtracking<utopia::USparseMatrix, utopia::UVector>>();
        // nlsolver.set_line_search_strategy(strategy);

        nlsolver.verbose(true);
        nlsolver.solve(st_monodomain, x_0);
    }
    else
    {
        std::cout << "--------- Apply direct solver -----------" << std::endl;
        Newton<USparseMatrix, UVector> nlsolver(linear_solver);
        nlsolver.verbose(true);
        nlsolver.solve(st_monodomain, x_0);
    }

    std::cout << "--------- solve finshed -----------" << std::endl;

    if (output && mc_flag) //transfer back
    {
        // x_0 = transfer_space_time_cr_fn * x_0;
        // io.write_time_block(st_op_fn, x_0);
        io.write_time_block(*st_op_ptr, x_0);
    }
    if (output && !mc_flag)
    {
        io.write_time_block(*st_op_ptr, x_0);
    }

    // save_vec(raw_type(x_0), "x_0.m", "sol");

    return 0;
}
