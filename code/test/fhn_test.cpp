#include "Space_timeMG.hpp"
#include "Laplace_assembler.hpp"
#include "Space_assembler.hpp"
#include "Eikonal_assembler.hpp"
#include "Utilities.hpp"
#include "libmesh/mesh_refinement.h"

using namespace libMesh;
using namespace utopia;
using std::make_shared;

UVector reaction_term(const UVector &u, const double alpha, const double min_potential, const double max_potential, const double unstable){

    auto size_loc = local_size(u).get(0);

    UVector u_min  = local_values(size_loc, min_potential);
    UVector u_max  = local_values(size_loc, max_potential);
    UVector u_unst = local_values(size_loc, unstable);

    return alpha * e_mul(e_mul(u - u_min, u - u_max), (u - u_unst));
}

UVector reaction_term_deriv(const UVector &u, const double alpha, const double min_potential, const double max_potential, const double unstable){

    auto size_loc = local_size(u).get(0);

    UVector u_min  = local_values(size_loc, min_potential);
    UVector u_max  = local_values(size_loc, max_potential);
    UVector u_unst = local_values(size_loc, unstable);

    return alpha * (e_mul(u - u_min, u - u_max) + e_mul(u - u_min, u - u_unst) + e_mul(u - u_unst, u - u_max));
}


int main(const int argc, char *argv[])
{
	libMesh::LibMeshInit init(argc, argv);

	if (argc < 3)
	{
		std::cerr << "WARNING: correct usage: ./test time_steps [levels_vector]" << std::endl;
		return 1;
	}

	// general setting
	const bool two_d = false;
	const bool output = false;

	// non linear solver setting
	const bool newton = true; // false is Picard
	int non_linear_iterations = 20;

	// FHN paramters
	double u_min = -85., u_max = 30., u_unst = -57.6, u_source = -0.5;
	double alpha = -1.4e-3;
	double chi   = 1400.;
	double diffusivity = 1.33;

	// time parameters
	double dt = 0.1;
	int time_blocks = 1;

	int boundary_tag = two_d? 1 : 5;
    // int boundary_tag = two_d? 1 : 104; // for HeartDimo_sidesets.e

    std::cout << "--------- mesh read -----------" << std::endl;

    auto mesh = make_shared<libMesh::DistributedMesh>(init.comm());

    if(two_d) {
        // const unsigned int nx = 40;
        // const unsigned int ny = 14;

        // libMesh::MeshTools::Generation::build_square(*mesh,
        //     nx, ny,
        //     0, 2.,
        //     0, 0.7,
        //     libMesh::QUAD4);
        mesh->read("../../data/rect2.1.e");
    } else {
        mesh->read("../../data/heart2.e");
        // mesh->read("../../data/HeartDimo_sidesets.e");
        //mesh->read("../../data/Myo_tet_only.e");

    }

    unsigned int n_refine = 0;

    if(n_refine > 0) {
        libMesh::MeshRefinement mesh_refinement(*mesh);
        mesh_refinement.make_flags_parallel_consistent();
        mesh_refinement.uniformly_refine(n_refine);
    }

    Laplace_assembler s_ass(mesh, false);
    s_ass.set_diff_coeff(diffusivity/chi);
    s_ass.assemble();

    auto s_ass_ptr = make_shared<Laplace_assembler>(s_ass);

    //time
    auto t_ass_ptr = make_shared<Time_slab_assembler>(dt);
    t_ass_ptr->assemble();

    // space-time
    auto st_op_ptr = make_shared<Space_time_assembler>(s_ass_ptr, t_ass_ptr, atoi(argv[1]));
    st_op_ptr->print_mu(diffusivity/chi);

    st_op_ptr->set_time_bc(boundary_tag, u_source, u_min, s_ass.get_fem_space());
    st_op_ptr->assemble();

    std::cout << "---------------------------------------------------" << std::endl;
    std::cout << "--------- space-time operator assembled -----------" << std::endl;
    std::cout << "---------------------------------------------------" << std::endl;

    // Be carefull for option like -g
    std::vector<int> levels;
    for (int i = 2; i < argc; i++){

        std::string str(argv[i]);
        if (str.size() <= 2) levels.push_back(atoi(argv[i]));
    }

    Space_timeMG st_mg(init, st_op_ptr, levels);
    st_mg.set_interpolation_flag(true);

    std::cout << "--------- space-time MG created -----------" << std::endl;

    // test solver
    //auto smoother = std::make_shared<GaussSeidel<USparseMatrix, UVector>>();
    auto smoother = std::make_shared<GMRES<USparseMatrix, UVector>>();
    //auto smoother = std::make_shared<PointJacobi<USparseMatrix, UVector>>();
    smoother->sweeps(3);

    // auto linear_solver = std::make_shared<BiCGStab<USparseMatrix, UVector>>();
    auto linear_solver = std::make_shared<Factorization<USparseMatrix, UVector>>();

    st_mg.multigrid = std::make_shared<Space_timeMG::Multigrid>(smoother, linear_solver);

    st_mg.multigrid->max_it(200);
    st_mg.multigrid->mg_type(1);
    // BiCGStab<USparseMatrix, UVector> bcg;
    // bcg.verbose(true);

    st_mg.init_mg<Laplace_assembler>();

    // bcg.set_preconditioner(st_mg.multigrid);

    // initial guess
    auto s_ST     = local_size(*(st_op_ptr->get_space_time_system()));
    UVector x_0  = local_zeros(s_ST.get(0));

    UVector correction  = local_zeros(s_ST.get(0));

    st_mg.multigrid->verbose(true);

    // build space_time identity
    st_op_ptr->set_space_time_mass();

    UVector I_ion = local_zeros(s_ST.get(0));
    UVector I_ion_coeff;

    UVector I_ion_deriv = local_zeros(s_ST.get(0));
    UVector I_ion_coeff_deriv;
    USparseMatrix I_ion_Jacobian;
    std::shared_ptr<USparseMatrix> correction_system;

    // set initial guess from heat eq.
    // st_mg.multigrid->apply(*(st_op_ptr->get_space_time_rhs()), x_0);

    std::cout << "--------- Solve Eikonal -----------" << std::endl;

    // init guess from eikonal, TODO before every time block?
    double tau = 30.;
    double c0 = 1.;

    {
    	Eikonal_assembler eikonal(mesh, boundary_tag, tau, c0);
    	eikonal.solve(0., 10);

    	eikonal_to_space_time(*st_op_ptr, *(eikonal.get_solution()), x_0, u_min, u_max);
	}


    // TODO destroy eikonal obj?

    // output file
    SpaceTimeIO io(*mesh, "../output/monodomain.e");

    // io.write_time_block(*st_op_ptr, x_0);
    // return 0;

    if(!newton) {
        st_mg.multigrid->update(st_op_ptr->get_space_time_system());
        st_mg.multigrid->describe(std::cout);
    }
    /////////////

    UVector x_old = x_0;

    for (int t = 0; t < time_blocks; ++t)
    {

        std::cout << " Time block number " << t << std::endl;

        for (int i = 0; i < non_linear_iterations; ++i)
        {
            if (newton)
            {
                I_ion_coeff = reaction_term(x_0, alpha, u_min, u_max, u_unst);
                I_ion = (*st_op_ptr->get_space_time_mass()) * I_ion_coeff;

                I_ion_coeff_deriv = reaction_term_deriv(x_0, alpha, u_min, u_max, u_unst);
                I_ion_deriv = (*st_op_ptr->get_space_time_mass()) * I_ion_coeff_deriv;

                I_ion_Jacobian = diag(I_ion_deriv);

                // solve for correction
                correction_system = std::make_shared<USparseMatrix>(*(st_op_ptr->get_space_time_system()) - I_ion_Jacobian);

                st_mg.multigrid->update(correction_system);
                st_mg.multigrid->describe(std::cout);
                st_mg.multigrid->apply(*(st_op_ptr->get_space_time_rhs()) + I_ion - I_ion_Jacobian*x_0, x_0);

            }
            else // Picard
            {
                // solve
                st_mg.multigrid->apply(*(st_op_ptr->get_space_time_rhs()) + I_ion, x_0);
                // gmres->apply(*(st_op_ptr.get_space_time_rhs()) + I_ion, x_0);

                // update rhs
                I_ion_coeff = reaction_term(x_0, alpha, u_min, u_max, u_unst);

                //double n_I = norm2(I_ion_coeff);
                //std::cout << " norm2(I_ion_coeff) = " << n_I << std::endl;
                I_ion = (*st_op_ptr->get_space_time_mass()) * I_ion_coeff;
            }

            double diff = norm2(x_0 - x_old);
            std::cout << "Nonlinear iteration: " << i << " diff: " << diff << std::endl;
            if(diff < 1e-8) {
                break;
            }

            x_old = x_0;
        }

        auto final_time = std::make_shared<UVector>(local_zeros(local_size(*st_op_ptr->get_time_bc())));
        extract_final_time(*st_op_ptr, x_0, *final_time);

        impose_initial_condition(*st_op_ptr, x_0);

        if (output)
        {
            io.write_time_block(*st_op_ptr, x_0);
        }

        //set new initial condition
        st_op_ptr->set_time_bc(final_time);
        st_op_ptr->assemble_rhs();

        copy_solution_to_next_time_block(*final_time, x_0);
    }

    std::cout << "--------- solve finshed -----------" << std::endl;

    // st_op_ptr.set_time_bc must be called


    // save_vec(raw_type(x_0), "solution.m", "sol");
    // save_solution(*st_op_ptr, *st_op_ptr->get_space_time_rhs()); // works just in serial

    return 0;
}
