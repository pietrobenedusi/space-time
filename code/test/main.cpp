#include "Laplace_assembler.hpp"
#include "Time_trapezoidal_assembler.hpp"
#include "Space_timeMG.hpp"
#include "Utilities.hpp"
#include "utopia_AppRunner.hpp"
#include <chrono>  // for high_resolution_clock
#include "Space_time_FHN_Model.hpp"
#include "FHN_Model.hpp"
#include "libmesh/mesh_refinement.h"




using namespace utopia;
using namespace libMesh;
using std::make_shared;


int main(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);
	PETSC_COMM_WORLD = MPI_COMM_WORLD;

	Utopia::Init(argc, argv);

	{
	    LibMeshInit init(argc, argv, PETSC_COMM_WORLD);

	    {
	        AppRunner app_runner;
	        app_runner.run(argc, argv);
	    }
	}

	return Utopia::Finalize();
}

static void heat(Input &in)
{

	PetscCommunicator u_comm;
	libMesh::Parallel::Communicator comm(u_comm.get());

	// input default params
	double D = 1.;
	double T = 1.;
	int N_t = 16;
	int q = 0;
	std::string mesh_path = "../data/circle_fn_fn.e"; 
	std::string diffusion_path ="../data/tensor_diffusion_cube_20.txt";
	int levels = 1;
	int coarsening_time = 2.;
	double coarsening_space = 2.;
	bool interpolation_flag = true;
	int smoothing_steps = 3;
	bool output = false;
	// forcing term
	double x0 = 0.0;
	double y0 = 0.0;
	double z0 = 0.0;
	double sigma = 1.5;
	double f_coeff = 4;
	// input from json
	in.get("D", D);
	in.get("T", T);
	in.get("N_t", N_t);
	in.get("q", q);
	in.get("mesh_path", mesh_path);
	in.get("diffusion_path", diffusion_path);
	in.get("levels", levels);
	in.get("coarsening_time", coarsening_time);
	in.get("coarsening_space", coarsening_space);
	in.get("interpolation_flag", interpolation_flag);
	in.get("smoothing_steps", smoothing_steps);
	in.get("output", output);
	in.get("x0", x0);
	in.get("y0", y0);
	in.get("z0", z0);
	in.get("sigma", sigma);
	in.get("f_coeff", f_coeff);

	// if no mesh is provided
	int Nx = 100;
	double l = 20;
	in.get("l", l);
	in.get("Nx", Nx);

	//start 
	auto start = std::chrono::high_resolution_clock::now();

	utopia::Utopia::instance().set("disable-adaptivity", "true");

	std::cout << "+++++++++++++" << std::endl;
	std::cout << "MPI size = "<< mpi_world_size() << std::endl;
	std::cout << "+++++++++++++" << std::endl;

	auto mesh = std::make_shared<DistributedMesh>(comm);
	// mesh->allow_renumbering(false);

	utopia::Chrono c;
	c.start();

	// super simple mesh for tests
	double dx = l/(Nx+1);

	// MeshTools::Generation::build_square(*mesh,
	// Nx, Nx,
	// -l, l,
	// -l, l,
	// //TET4);
	// QUAD4);

	// MeshTools::Generation::build_line(*mesh, Nx, 0, l, EDGE2);

	mesh->read(mesh_path);

    std::cout << "--------- Mesh read -----------" << std::endl;

    auto s_ass_ptr = make_shared<Laplace_assembler>(mesh, false);
    // s_ass_ptr->set_mass_lumping();

    // vector diffusion 
	// s_ass_ptr->set_diff_coeff(diffusion_path);

	// scalar diffusion    
	s_ass_ptr->set_diff_coeff(D);

    // tensor diffusion
    // s_ass_ptr->set_diff_tensor(diffusion_path);

    // jump diffusion
    // s_ass_ptr->set_jump_diff();
	
	s_ass_ptr->assemble();

	save_mat(raw_type(*s_ass_ptr->get_stiff()), "../output/K_t.m", "Kt");
	// save_mat(raw_type(*s_ass_ptr->get_mass()), "../output/Mcc.m", "Mc");	

	std::cout << "--------- space operator assembled -----------" << std::endl;

	// for (int i = 5; i < 6; ++i)
	// {
		// c.start();

		// double N_t = pow(2,i) + 1;
		// double N_t = 64 * mpi_world_size() + 1;		
		double dt = T/N_t;

		std::cout << "==============================" << std::endl;
		std::cout << "======== dt = " << dt << std::endl;
		// std::cout << "======== dx = " << dx << std::endl;
		std::cout << "======== D = " << D << std::endl;
		std::cout << "======== N_t = " << N_t << " ============= "<< std::endl;
		std::cout << "==============================" << std::endl;

		// std::cout << "Total dofs =  " << 2 * N_t * Nx * Nx  << std::endl;
		// std::cout << "mu =  " << D * dt / (dx * dx)  << std::endl;

		auto t_ass_ptr = make_shared<Time_slab_assembler>(dt, q + 1);
		// auto t_ass_ptr = make_shared<Time_trapezoidal_assembler>(dt);
		t_ass_ptr->assemble();

		std::cout << "--------- time operator assembled -----------" << std::endl;

		// space time

		auto st_op_ptr = make_shared<Space_time_assembler>(s_ass_ptr, t_ass_ptr, N_t);
		st_op_ptr->print_mu(D);


		// bool set_compute_prec_ = false;
		// st_op_ptr->set_compute_prec(set_compute_prec_);

		// set time BC
		// // auto time_bc = std::make_shared<UVector>(zeros(st_op_ptr.get_size_A()));

		// bool initial_bc = false;
		// st_op_ptr->set_time_bc(0., initial_bc);

	    UVector inital_forcing;
	    set_gaussian_forcing(*s_ass_ptr, inital_forcing, f_coeff, 0., sigma, x0, y0, z0);  // <------------ attenzione qui!
	    auto inital_forcing_ptr = make_shared<UVector>(inital_forcing);
    	st_op_ptr->set_time_bc(inital_forcing_ptr);

        // st_op_ptr->set_time_bc(boundary_tag, 10., 0., s_ass_ptr->get_fem_space());

		st_op_ptr->assemble();

		// destroy stuff?
		// MatDestroy(&raw_type(*s_ass_ptr->get_stiff()));

		// save_mat(raw_type(*st_op_ptr->get_space_time_system()), "../output/C_c.m", "C");
		// save_vec(raw_type(*st_op_ptr->get_space_time_rhs()), "../output/rhs_c.m", "rhs");

		c.stop();
		utopia::mpi_world_barrier();
		if(utopia::mpi_world_rank() == 0)   std::cout << "Space-Time operator assembly: " << c << std::endl;

		mpi_world_barrier();
		std::cout << "--------- space-time operator assembled -----------" << std::endl;

				
		if (levels > 1)
		{
			// MG set up
			c.start();
			std::cout << levels << " MG levels" << std::endl;

			Space_timeMG st_mg(comm, st_op_ptr, levels);
			st_mg.set_interpolation_flag(interpolation_flag);
			st_mg.set_coarsening_space(coarsening_space);
			st_mg.set_coarsening_time(coarsening_time);
			st_mg.set_smoothing_steps(smoothing_steps);


			// // set spatial meshes manually
			// std::vector< std::shared_ptr<libMesh::DistributedMesh> > spatial_meshes;
			// spatial_meshes.reserve(levels - 1);
			
			// auto mesh_cr1 = std::make_shared<DistributedMesh>(comm);
			// mesh_cr1->read("../data/cylinder_fn.e");	
			// spatial_meshes.push_back(mesh_cr1);
			
			// if (levels > 2)
			// {
			// 	auto mesh_cr2 = std::make_shared<DistributedMesh>(comm);
			// 	mesh_cr2->read("../data/cylinder_cr.e");	
			// 	spatial_meshes.push_back(mesh_cr2);
			// }

			// if (levels > 3)
			// {
			// 	auto mesh_cr3 = std::make_shared<DistributedMesh>(comm);
			// 	mesh_cr3->read("/Users/pietro/Desktop/meshes/geo2.e");	
			// 	spatial_meshes.push_back(mesh_cr3);
			// }

			// if (levels > 4)
			// {
			// 	auto mesh_cr4 = std::make_shared<DistributedMesh>(comm);
			// 	mesh_cr4->read("/Users/pietro/Desktop/meshes/geo1.e");	
			// 	spatial_meshes.push_back(mesh_cr4);
			// }

			// if (levels > 5)
			// {
			// 	auto mesh_cr5 = std::make_shared<DistributedMesh>(comm);
			// 	mesh_cr5->read("/Users/pietro/Desktop/meshes/geo0.e");	
			// 	spatial_meshes.push_back(mesh_cr5);
			// }

			// st_mg.set_spatial_meshes(spatial_meshes);
								
						
			std::cout << "--------- space-time MG created -----------" << std::endl;

			st_mg.init_mg<Laplace_assembler>();		// TODO con false non funziona
			st_mg.print_mu(D);
			st_mg.multigrid->max_it(3000);

			std::cout << "--------- MG init done -----------" << std::endl;	

			c.stop();
			utopia::mpi_world_barrier();
			if(utopia::mpi_world_rank() == 0) 	std::cout << "MG init time: " << c << std::endl;


			// initial guess
			auto s_ST     = local_size(*(st_op_ptr->get_space_time_system()));
			UVector x_0  = local_zeros(s_ST.get(0));

			// solve
			c.start();
			st_mg.solve(x_0);
			c.stop();
			utopia::mpi_world_barrier();
			if(utopia::mpi_world_rank() == 0) 	std::cout << "Solve time: " << c << std::endl;

			// ierr = KSPDestroy(&solver);CHKERRV(ierr);

			if (output)
			{
				SpaceTimeIO io(*mesh, "../output/heat.e");
		  		io.write_time_block(*st_op_ptr, x_0);
			}
			
			// save_vec(raw_type(x_0), "../output/x_0.m", "sol");
		}
		else
		{
			auto s_ST    = local_size(*(st_op_ptr->get_space_time_system()));
			UVector x_0  = local_zeros(s_ST.get(0));

			// GMRES<USparseMatrix, UVector> gmres;
			// gmres.verbose(true);
			// gmres.max_it(3000);
			// gmres.pc_type("bjacobi");
			// gmres.describe(std::cout);

			// c.start();
			// gmres.update(st_op_ptr->get_space_time_system());
			// c.stop();

			// if(utopia::mpi_world_rank() == 0) 	std::cout << "GMRES set up time: " << c << std::endl;

			// c.start();
			// gmres.apply(*st_op_ptr->get_space_time_rhs(), x_0);
			// c.stop();

			// I do this because dunno how to get number of its with utopia
			c.start();
			PetscErrorCode ierr;
			KSP solver;
			ierr = KSPCreate(PETSC_COMM_WORLD,&solver);CHKERRV(ierr);
			ierr = KSPSetType(solver,KSPGMRES);CHKERRV(ierr);
			ierr = KSPSetOperators(solver, raw_type(*st_op_ptr->get_space_time_system()),
												 raw_type(*st_op_ptr->get_space_time_system()));CHKERRV(ierr);

			ierr = KSPSetTolerances(solver,1e-9,1e-9,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRV(ierr);
			ierr = KSPSetUp(solver);CHKERRV(ierr);
			c.stop();

			if(utopia::mpi_world_rank() == 0) 	std::cout << "GMRES set up time: " << c << std::endl;

			c.start();
			ierr = KSPSolve(solver, raw_type(*st_op_ptr->get_space_time_rhs()), raw_type(x_0));CHKERRV(ierr);
			c.stop();

			int its;
			KSPGetIterationNumber(solver,&its);
			std::cout << "GMRES iterations = " << its << std::endl;

			ierr = KSPDestroy(&solver);CHKERRV(ierr);

			utopia::mpi_world_barrier();
			if(utopia::mpi_world_rank() == 0) 	std::cout << "GMRES solve time: " << c << std::endl;
			
			if (output)
			{
				SpaceTimeIO io(*mesh, "../output/heat.e");
		  		io.write_time_block(*st_op_ptr, x_0);
			}
		}

		std::cout << "--------- solve finshed -----------" << std::endl;

		// UVector x_space = local_zeros(s_ass_ptr->get_size());
		// extract_nth_time(*st_op_ptr, x_0, x_space, 4, 0);
		// extract_nth_time(*st_op_ptr, x_0, x_space, 4, 0);

		// st_op_ptr.set_time_bc must be called
		//impose_initial_condition(*st_op_ptr, x_0);		

		utopia::mpi_world_barrier();

		auto finish = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> elapsed = finish - start;
		std::cout << "Elapsed time: " << elapsed.count() << " s\n";
	// }
}

UTOPIA_REGISTER_APP(heat);


static void monodomain(Input &in)
{
    PetscCommunicator u_comm;
	libMesh::Parallel::Communicator comm(u_comm.get());

	// input default params
	double D = 1.;
	double T = 1.;
	int N_t = 16;
	int time_blocks = 10;
	int q = 0;
	std::string mesh_path = "../data/circle_fn_fn.e"; 
	std::string diffusion_path ="../data/tensor_diffusion_cube_20.txt";
	int levels = 1;
	int coarsening_time = 2.;
	double coarsening_space = 2.;
	bool interpolation_flag = true;
	int smoothing_steps = 3;
	bool output = false;
	// forcing term
	double x0 = 0.0;
	double y0 = 0.0;
	double z0 = 0.0;
	double sigma = 1.5;
	double f_coeff = 4;

	// FHN params
	// double u_min = -85., u_max = 30., u_unst = -57.6, u_source = 0.;
    double u_min = -1, u_max = 1., u_unst = -0.8;
    double alpha = -5.0;
    double nl_tol = 0.0;
    int max_it = 200;
    bool implicit_flag = true;
    bool lin_verbose = false;

	// input from json
	in.get("D", D);
	in.get("T", T);
	in.get("N_t", N_t);
	in.get("time_blocks", time_blocks);
	in.get("q", q);
	in.get("mesh_path", mesh_path);
	in.get("diffusion_path", diffusion_path);
	in.get("levels", levels);
	in.get("coarsening_time", coarsening_time);
	in.get("coarsening_space", coarsening_space);
	in.get("interpolation_flag", interpolation_flag);
	in.get("smoothing_steps", smoothing_steps);
	in.get("output", output);
	in.get("x0", x0);
	in.get("y0", y0);
	in.get("z0", z0);
	in.get("sigma", sigma);
	in.get("f_coeff", f_coeff);
	in.get("u_min", u_min);
	in.get("u_max", u_max);
	in.get("u_unst", u_unst);
	in.get("alpha", alpha);	
	in.get("nl_tol", nl_tol);
	in.get("max_it", max_it);
	in.get("implicit_flag", implicit_flag);
	in.get("lin_verbose", lin_verbose);

    utopia::Utopia::instance().set("disable-adaptivity", "true");

    double dt = T/N_t;
    std::cout << "D = " << D << std::endl;
    std::cout << "T = " << T << std::endl;
    std::cout << "dt = " << dt << std::endl;
    std::cout << "u_min = " << u_min << std::endl;  
    std::cout << "u_max = " << u_max << std::endl; 
    std::cout << "u_unst = " << u_unst << std::endl;  
    std::cout << "alpha = " << alpha << std::endl;  
    std::cout << "implicit_flag = " << implicit_flag << std::endl;
    
    double c = (-2./(u_max - u_min))*(2./(u_max - u_min)/alpha);
    if (dt > c) {
        std::cout << "WARNING: dt may be too large for current reaction" << std::endl;
        std::cout << "set dt < " <<  c << std::endl;
    }

    std::cout << "--------- Read mesh -----------" << std::endl;

    auto mesh = std::make_shared<DistributedMesh>(comm);
	// mesh->allow_renumbering(false);
	mesh->read(mesh_path);

    std::cout << "--------- Assemble space and time operators -----------" << std::endl;

    // space
    Laplace_assembler s_ass(mesh, false);
    s_ass.set_diff_coeff(D);
    // s_ass.set_diff_tensor("../data/tensor_diffusion_cube_20.txt");
    s_ass.assemble();

    auto s_ass_ptr = make_shared<Laplace_assembler>(s_ass);

    // time
    // auto t_ass_ptr = make_shared<Time_trapezoidal_assembler>(dt);
    auto t_ass_ptr = make_shared<Time_slab_assembler>(dt, q + 1);
    t_ass_ptr->assemble();

    // space-time
    auto st_op_ptr = make_shared<Space_time_assembler>(s_ass_ptr, t_ass_ptr, N_t);
    st_op_ptr->print_mu(D);

    // set time bc troug forcing factor
    UVector inital_forcing;
    set_gaussian_forcing(s_ass, inital_forcing, f_coeff * u_max, u_min, sigma, x0, y0, z0);
    auto inital_forcing_ptr = make_shared<UVector>(inital_forcing);
    st_op_ptr->set_time_bc(inital_forcing_ptr);
    
    st_op_ptr->assemble();

    std::cout << "Total #dofs = " << st_op_ptr->get_size_ST() << std::endl;

    std::cout << "--------- Space time operators assembled -----------" << std::endl;
  
    // initial guess
    auto s_ST    = local_size(*(st_op_ptr->get_space_time_system()));
    UVector x_0  = local_values(s_ST.get(0), u_min);
    // UVector x_0  = local_zeros(s_ST.get(0));
    
    SpaceTimeIO io(*mesh, "../output/monodomain.e");

    // monodomain
    Space_time_FHN_Model<LibMeshFunctionSpace, USparseMatrix, UVector> st_monodomain(st_op_ptr, s_ass.get_fem_space(), 
    																	alpha, u_min, u_max, u_unst, implicit_flag);

    std::cout << "--------- Monodomain set -----------" << std::endl;

    // auto gmres = std::make_shared<GMRES<USparseMatrix, UVector>>();    
    // gmres->pc_type("bjacobi");
    // gmres->verbose(lin_verbose);        
    // Newton<USparseMatrix, UVector> nlsolver(gmres);
    
    std::cout << "MG levels = " << levels << std::endl;
    Space_timeMG st_mg(comm, st_op_ptr, levels);
	st_mg.set_interpolation_flag(interpolation_flag);
	st_mg.set_coarsening_space(coarsening_space);
	st_mg.set_coarsening_time(coarsening_time);
	st_mg.set_smoothing_steps(smoothing_steps);

	// set spatial meshes manually
	std::vector< std::shared_ptr<libMesh::DistributedMesh> > spatial_meshes;
	spatial_meshes.reserve(levels - 1);
	
	auto mesh_cr1 = std::make_shared<DistributedMesh>(comm);
	mesh_cr1->read("/Users/pietro/Desktop/meshes/geo0.e");	
	spatial_meshes.push_back(mesh_cr1);
	
	if (levels > 2)
	{
		auto mesh_cr2 = std::make_shared<DistributedMesh>(comm);
		mesh_cr2->read("/Users/pietro/Desktop/meshes/geo1.e");	
		spatial_meshes.push_back(mesh_cr2);
	}

	if (levels > 3)
	{
		auto mesh_cr3 = std::make_shared<DistributedMesh>(comm);
		mesh_cr3->read("/Users/pietro/Desktop/meshes/geo0.e");	
		spatial_meshes.push_back(mesh_cr3);
	}

	if (levels > 4)
	{
		auto mesh_cr4 = std::make_shared<DistributedMesh>(comm);
		mesh_cr4->read("/Users/pietro/Desktop/meshes/geo0.e");	
		spatial_meshes.push_back(mesh_cr4);
	}

	if (levels > 5)
	{
		auto mesh_cr5 = std::make_shared<DistributedMesh>(comm);
		mesh_cr5->read("/Users/pietro/Desktop/meshes/geo0.e");	
		spatial_meshes.push_back(mesh_cr5);
	}

	st_mg.set_spatial_meshes(spatial_meshes);
						
	st_mg.init_mg<Laplace_assembler>();
    st_mg.print_mu(D);

    Newton<USparseMatrix, UVector> nlsolver(st_mg.multigrid);
    st_mg.multigrid->verbose(lin_verbose); 


    if (nl_tol > 0)
    {
    	nlsolver.atol(nl_tol);
	    nlsolver.rtol(nl_tol);
    	nlsolver.stol(nl_tol);
    }

    nlsolver.max_it(max_it);
    nlsolver.verbose(true);

    // if (abs(alpha) > 0.001) nlsolver.forcing_strategy(InexactNewtonForcingStartegies::SUPERLINEAR);
    // nlsolver.forcing_strategy(InexactNewtonForcingStartegies::SUPERLINEAR);    
    // if (abs(alpha) > 0.001) nlsolver.forcing_strategy(InexactNewtonForcingStartegies::QUADRATIC);
    // if (abs(alpha) > 0.001) nlsolver.forcing_strategy(InexactNewtonForcingStartegies::SUPERLINEAR);



    for (int i = 0; i < time_blocks; ++i)
    {

        std::cout << std::endl << " Time block number " << i << std::endl << std::endl;
       
        nlsolver.solve(st_monodomain, x_0);
        
        if (output) io.write_time_block(*st_op_ptr, x_0);

        // save_mat(raw_type(*st_op_ptr->get_space_time_system()), "../output/system.m", "ST");
        // save_vec(raw_type(x_0), "../output/x_0.m", "sol");

        if (i < time_blocks - 1)
        {

            // copy to next block // TODO questo bene
            auto final_time = std::make_shared<UVector>(local_zeros(local_size(*st_op_ptr->get_time_bc())));
            extract_final_time(*st_op_ptr, x_0, *final_time);

            // scale by mass matrix
            auto mass = st_op_ptr->get_space_op()->get_mass();

            *final_time = (*mass) * (*final_time);

            //set new initial condition
            st_op_ptr->set_time_bc(final_time, false);
            st_op_ptr->assemble_rhs();

            copy_solution_to_next_time_block(*st_op_ptr, *final_time, x_0);               
        }
    }
}

UTOPIA_REGISTER_APP(monodomain);

