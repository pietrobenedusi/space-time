#include "Space_timeMG.hpp"
#include "Space_time_FHN_Model.hpp"
#include "Time_trapezoidal_assembler.hpp"
#include "Laplace_assembler.hpp"
#include "Eikonal_assembler.hpp"
#include "Utilities.hpp"
using namespace libMesh;
using namespace utopia;
using std::make_shared;
int main(const int argc, char *argv[])
{
    libMesh::LibMeshInit init(argc, argv);

    // Mesh filename
    std::string mesh_filename;

    // input check
    bool pp;

    // montecarlo stuff
    int mc_level = 0;

    const bool output = true;

    // ------------- problem parameters ---------------- //

    // FHN paramters
    double u_min = 0., u_max = 115., u_unst = 28., u_source = 110.;
    double alpha = -1.4e-3;
    double chi   = 400.;

    // for gaussian initial condition
    double sigma = 0.3;
    double x0 = 0.;
    double y0 = 0.;
    double z0 = 0.;

    // time parameters
    double dt = 0.1;
    int time_steps;
    int time_blocks = 1;

    // reading input
    if (argc < 2)
    {
        std::cerr << "ERROR: correct usage: ./st_mc -pp mesh_filename" << std::endl;
        std::cerr << "                      ./st_mc -solve time_steps mesh_filename sample_filename [mc_level]" << std::endl;
        return 1;
    }
    if (strcmp(argv[1], "-pp") == 0)
    {
        pp = true;
        mesh_filename = argv[2];
    }
    else if (strcmp(argv[1], "-solve") == 0){

        pp = false;
        time_steps = atoi(argv[2]);
        mesh_filename = argv[3];

        if ( argc == 6)
        {
            mc_level = atoi(argv[5]);

            if (mc_level < 1)
            {
                std::cerr << "ERROR: MC level mc_l should satisfy: 1 <= mc_l" << std::endl;
                return 1;
            }
        }
        if (argc != 5 && argc != 6)
        {
            std::cerr << "ERROR: some argument is missing" << std::endl;
            std::cerr << "USAGE: ./st_mc -solve time_steps mesh_filename sample_filename" << std::endl;
            return 1;
        }
    }
    else {
        std::cerr << "ERROR: second argument can be -pp for preprocessing or -solve for solving." << std::endl;
        return 1;
    }

    // mesh
    auto mesh = make_shared<libMesh::DistributedMesh>(init.comm());

    mesh->allow_renumbering(false);
    // mesh->read(mesh_filename);
    MeshTools::Generation::build_line(*mesh, 100, 0, 1, EDGE2);

    Laplace_assembler s_ass(mesh, false);

    if (pp)
    {
        // save Dof's
        save_dofs_coord(s_ass, mesh);

        // create mass matrix
        s_ass.assemble();
        save_mat(raw_type(*s_ass.get_mass()),"log_hessian.m","H");
        return 0;
    }

    auto &dof_map = s_ass.get_dof_map();
    utopia::UIndexArray ghost_nodes;
    convert(dof_map.get_send_list(), ghost_nodes);

    auto diff_vec = std::make_shared<utopia::UVector>(utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes));

    if (!pp) // get diffusion from file
    {
        //std::string line;
        std::ifstream myfile;
        std::cout << "Reading file " << argv[4] << std::endl;
        myfile.open(argv[4]);

        double d;
        int counter = 0;
        auto range_ = range(*diff_vec);
        if (myfile.is_open())
        {
            {
                Write<UVector> w(*diff_vec);

                while ( myfile >> d )
                {
                    if (counter == s_ass.get_size())
                    {
                        std::cout << "ERROR: input file too big!"<< '\n'; //TODO check for blanks?
                        std::cout << "counter = " << counter << '\n'; //TODO check for blanks?
                        return 1;
                    }
                    if(range_.inside(counter))
                    {
                        diff_vec->set(counter, d);
                    }
                    counter++;
                }
                myfile.close();
            }
        }
        else{
            std::cout << "Unable to open file"<< '\n';
            return 1;
        }
        if (counter != s_ass.get_size())
        {
            std::cout << "ERROR: input file has wrong size!"<< '\n';
            std::cout << "Counter = " << counter << '\n';
            std::cout << "DoFs = " << s_ass.get_size() << '\n';
            return 1;
        }


        // ============================================== //
        *diff_vec *= (1./chi);
        utopia::synchronize(*diff_vec);

        UVector    diff_vec_seq;
        VecScatter     ctx;

        PetscErrorCode ierr;

        ierr = VecScatterCreateToAll(raw_type(*diff_vec), &ctx, &raw_type(diff_vec_seq)); // TODO not to all?
        ierr = VecScatterBegin(ctx, raw_type(*diff_vec), raw_type(diff_vec_seq),INSERT_VALUES,SCATTER_FORWARD);
        ierr = VecScatterEnd(ctx, raw_type(*diff_vec), raw_type(diff_vec_seq),INSERT_VALUES,SCATTER_FORWARD);
        ierr = VecScatterDestroy(&ctx);
        ierr = VecAssemblyBegin(raw_type(diff_vec_seq));
        ierr = VecAssemblyEnd(raw_type(diff_vec_seq));

        {
            Write<UVector> w(*diff_vec);
            Read<UVector> r(diff_vec_seq);

            for(auto n_it = mesh->local_nodes_begin(); n_it != mesh->local_nodes_end(); ++n_it)
            {
                auto &node = **n_it;
                auto dof_id = node.dof_number(s_ass.get_equation_system().number(), 0, 0);
                diff_vec->set(dof_id,diff_vec_seq.get(node.id()));
            }
        }

        VecDestroy(&raw_type(diff_vec_seq));

        // If -solve continue
        s_ass.set_diff_coeff(diff_vec);
        s_ass.assemble(); // TODO is this necessary for mc? not really

        std::cout << "--------- space operator assembled -----------" << std::endl;
    }

    auto s_ass_ptr = make_shared<Laplace_assembler>(s_ass);

    // time
    // auto t_ass_ptr = make_shared<Time_slab_assembler>(dt);
    auto t_ass_ptr = make_shared<Time_trapezoidal_assembler>(dt);

    if (!pp && !mc_level)
    {
        t_ass_ptr->assemble();
        std::cout << "--------- time operator assembled -----------" << std::endl;
    }

    // space-time
    auto st_op_ptr = make_shared<Space_time_assembler>(s_ass_ptr, t_ass_ptr, time_steps);

    // copy of unassembled space-time op for later use
    auto st_op_fn = *st_op_ptr;

    if (!pp) // this is not necassary for pp
    {
        if (!mc_level)
        {
            // set initial condition
            auto inital_forcing_ptr = make_shared<UVector>();
            set_gaussian_forcing(s_ass, *inital_forcing_ptr, u_source - u_min, u_min, sigma, x0, y0, z0);
            st_op_ptr->set_time_bc(inital_forcing_ptr);

            // assemble
            st_op_ptr->assemble();
            std::cout << "--------- space-time operator assembled -----------" << std::endl;
        }
    }

    USparseMatrix transfer_space_fn_cr;
    USparseMatrix transfer_space_time_cr_fn, transfer_space_time_fn_cr;
    auto mesh_mc = make_shared<libMesh::DistributedMesh>(init.comm());

    if (mc_level) // Montecarlo stuff
    {
        std::cout << "--------- Creating Montecarlo mesh... -----------" << std::endl;

        const auto dim = s_ass_ptr->get_mesh()->mesh_dimension();

        // compute elements per side in the coarse mesh
        int elem_per_side = ceil(cbrt(s_ass_ptr->get_size()));
        elem_per_side = (int) ceil(elem_per_side/pow(2,mc_level));

        // compute bounding box of the fine mesh
        auto bounding_box = MeshTools::create_bounding_box(*mesh);
        auto box_min = bounding_box.min();
        auto box_max = bounding_box.max();

        double mean_box = 0;

        for (int i = 0; i < dim; ++i)
        {
            mean_box += box_max(i) - box_min(i);
        }
        mean_box = mean_box/dim;

        double scaling_0 = ((box_max(0) - box_min(0))/mean_box);
        double scaling_1 = ((box_max(1) - box_min(1))/mean_box);
        double scaling_2 = ((box_max(2) - box_min(2))/mean_box);

        if (dim == 3)
        {
           MeshTools::Generation::build_cube(*mesh_mc,
                                          round(scaling_0 * elem_per_side), round(scaling_1 * elem_per_side), round(scaling_2 * elem_per_side),
                                          box_min(0), box_max(0),
                                          box_min(1), box_max(1),
                                          box_min(2), box_max(2),
                                          HEX8);
        }
        else if(dim == 1)
        {
            // MeshTools::Generation::build_line(*mesh_mc,
            //                               round(scaling_0 * elem_per_side),
            //                               box_min(0), box_max(0),
            //                               EDGE2);

            MeshTools::Generation::build_line(*mesh_mc, 51, 0, 1, EDGE2);
        }

        mesh_mc->allow_renumbering(false);

        // coarse time mesh
        int time_steps_cr = 1 + (time_steps - 1)/pow(2,mc_level);
        double dt_mc = dt * pow(2,mc_level);

        std::cout << "--------- Creating coarse operator... -----------" << std::endl;

        auto s_ass_cr_ptr = make_shared<Laplace_assembler>(mesh_mc, false);

        std::cout << "--------- Building transfer operators... -----------" << std::endl;

        USparseMatrix transfer_space, transfer_time, transfer_space_fn_cr_tmp;

        // build spatial transfer fn to cr
        coupling_space(init, *s_ass_ptr, *s_ass_cr_ptr, transfer_space_fn_cr_tmp, false, false);
        transfer_space_fn_cr = transpose(transfer_space_fn_cr_tmp);

        // build spatial transfer cr to fn
        coupling_space(init, *s_ass_ptr, *s_ass_cr_ptr, transfer_space, true, false);

        for (int i = 0; i < mc_level; ++i) // create temporal restriction and assemble space-time transfer
        {
            USparseMatrix transfer_time_tmp, transfer_space_time_slab;

            int time_steps_interm_fn = 1 + (time_steps - 1)/pow(2,i);
            int time_steps_interm_cr = 1 + (time_steps - 1)/pow(2,i+1);

            coupling_time(time_steps_interm_fn, time_steps_interm_cr, raw_type(transfer_time_tmp));

            if (i == 0)
            {
                transfer_time = transfer_time_tmp;
            }
            else
            {
                transfer_time *= transfer_time_tmp;
            }
            if (i == mc_level - 1)
            {
                auto time_slab_size = t_ass_ptr->get_size();

                USparseMatrix I_time_slab = identity(time_slab_size,time_slab_size);

                PetscErrorCode ierr;

                // CR to FN
                ierr = kron(raw_type(transfer_space), raw_type(I_time_slab), raw_type(transfer_space_time_slab));CHKERRQ(ierr);
                ierr = kron(raw_type(transfer_time), raw_type(transfer_space_time_slab), raw_type(transfer_space_time_cr_fn));CHKERRQ(ierr);

                //FN to CR
                transfer_time = (2./3.) * transpose(transfer_time);

                // USparseMatrix blbla = transpose(transfer_space_fn_cr);
                ierr = kron(raw_type(transfer_space_fn_cr), raw_type(I_time_slab), raw_type(transfer_space_time_slab));CHKERRQ(ierr);
                ierr = kron(raw_type(transfer_time), raw_type(transfer_space_time_slab), raw_type(transfer_space_time_fn_cr));CHKERRQ(ierr);
            }
        }

        // for fn to cr l2 proj is used
        std::cout << "--------- Transfer diffusion -----------" << std::endl;

        // transfer diffusion
        auto &dof_map = s_ass_cr_ptr->get_dof_map();

        utopia::UIndexArray ghost_nodes;
        convert(dof_map.get_send_list(), ghost_nodes);

        UVector diff_vec_mc = utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes);
        diff_vec_mc = transfer_space_fn_cr * (*diff_vec);
        auto diff_vec_mc_ptr = std::make_shared<utopia::UVector>(diff_vec_mc);
        utopia::synchronize(*diff_vec_mc_ptr);

        std::cout << "--------- Assembling coarse problem... -----------" << std::endl;

        // coarse space
        s_ass_cr_ptr->set_diff_coeff(diff_vec_mc_ptr);
        s_ass_cr_ptr->assemble();

        // coarse time
        // auto t_ass_cr_ptr = make_shared<Time_slab_assembler>(dt_mc);
        auto t_ass_cr_ptr = make_shared<Time_trapezoidal_assembler>(dt_mc);
        t_ass_cr_ptr->assemble();
        // auto time_bc_tmp = st_op_ptr->get_time_bc();

        // coarse space-time
        st_op_ptr = make_shared<Space_time_assembler>(s_ass_cr_ptr, t_ass_cr_ptr, time_steps_cr);

        // coarse time bc
        auto inital_forcing_ptr = make_shared<UVector>();
        set_gaussian_forcing(*s_ass_cr_ptr, *inital_forcing_ptr, u_source - u_min, u_min, sigma, x0, y0, z0);
        st_op_ptr->set_time_bc(inital_forcing_ptr);

        // assemble
        st_op_ptr->assemble();

        s_ass = *s_ass_cr_ptr;
    }

    // initial guess
    auto s_ST = local_size(*(st_op_ptr->get_space_time_system())); // this is present just for assembled? check this
    UVector x_0  = local_zeros(s_ST.get(0));


    // output file
    SpaceTimeIO io(*mesh, "../output/monodomain.e");
    // SpaceTimeIO io_cr(*mesh_mc, "../output/monodomain_cr.e");

    for (int t = 0; t < time_blocks; ++t)
    {
        std::cout << std::endl << " Time block number " << t << std::endl << std::endl;

        // solve
        Space_time_FHN_Model<LibMeshFunctionSpace, USparseMatrix, UVector> st_monodomain(st_op_ptr, s_ass.get_fem_space(),
                                                                                  alpha, u_min, u_max, u_unst);


        // solve monodomain
        auto gmres = std::make_shared<GMRES<USparseMatrix, UVector>>();
        gmres->max_it(1000);
        Newton<USparseMatrix, UVector> nlsolver(gmres);
        nlsolver.verbose(true);

        nlsolver.solve(st_monodomain, x_0);


        // int start, end;
        // VecGetOwnershipRange(raw_type(x_0),&start,&end);

        // std::cout << "start, end = " << start << " ," << end << std::endl;

        // UVector  x_0_ord;
        // VecScatter     ctx;
        // int ierr;

        // // // ierr = VecScatterCreateToAll(raw_type(x_0), &ctx, &raw_type(x_0_seq)); // TODO not to all?
        // // // ierr = VecScatterBegin(ctx, raw_type(x_0), raw_type(x_0_seq),INSERT_VALUES,SCATTER_FORWARD);
        // // // ierr = VecScatterEnd(ctx, raw_type(x_0), raw_type(x_0_seq),INSERT_VALUES,SCATTER_FORWARD);
        // // // ierr = VecScatterDestroy(&ctx);
        // // // ierr = VecAssemblyBegin(raw_type(x_0_seq));
        // // // ierr = VecAssemblyEnd(raw_type(x_0_seq));

        // ierr = VecCreateSeq(PETSC_COMM_SELF,s_ass.get_size(), &raw_type(x_0_ord));



        // {
        //     Write<UVector> w(x_0_ord);
        //     Read<UVector> r(x_0);

        //     for(auto n_it = mesh->local_nodes_begin(); n_it != mesh->local_nodes_end(); ++n_it)
        //     {
        //         auto &node = **n_it;
        //         auto dof_id = node.dof_number(s_ass.get_equation_system().number(), 0, 0);

        //         // std::cout << "node.id() = " << node.id() << std::endl;
        //         // std::cout << "dof_id = " << dof_id << std::endl;

        //         if (start <= node.id() && node.id() < end)
        //         {
        //             x_0_ord.set(dof_id,x_0.get(node.id()));
        //         }
        //     }
        // }

        save_vec(raw_type(x_0), "../output/x_0.m", "sol1");

        // VecDestroy(&raw_type(x_0_ord));

        std::cout << "--------- solve finshed -----------" << std::endl;
        MPI_Barrier(MPI_COMM_WORLD);


        // copy to next block
        auto final_time = std::make_shared<UVector>(local_zeros(local_size(*st_op_ptr->get_time_bc())));

        extract_final_time(*st_op_ptr, x_0, *final_time);

        if (output && mc_level) //transfer back
        {
            // io_cr.write_time_block(*st_op_ptr, x_0);

            UVector x_0_fine = transfer_space_time_cr_fn * x_0;
            io.write_time_block(st_op_fn, x_0_fine);
        }
        if (output && !mc_level)
        {
            io.write_time_block(*st_op_ptr, x_0);
        }

        if (t < time_blocks - 1)
        {
            // scale by mass matrix
            auto mass = st_op_ptr->get_space_op()->get_mass();

            *final_time = (*mass) * (*final_time);

            //set new initial condition
            st_op_ptr->set_time_bc(final_time, false);
            st_op_ptr->assemble_rhs();

            copy_solution_to_next_time_block(*st_op_ptr, *final_time, x_0);
        }
    }

    return 0;
}
