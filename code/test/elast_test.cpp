#include "Space_timeMG.hpp"
#include "Elasticity_assembler.hpp"
#include "Utilities.hpp"

using namespace utopia;
using std::make_shared;

int main(const int argc, char *argv[])
{
	if (argc < 3)
	{
		std::cerr << "WARNING: correct usage: ./test time_steps levels" << std::endl;
		return 1;
	}

	libMesh::LibMeshInit init(argc, argv);
	// space
	auto s_ass_ptr = make_shared<Elasticity_assembler>(init, "../../data/cylinder.e", true);
	s_ass_ptr->assemble();

	// time
	auto t_ass_ptr = make_shared<Time_slab_assembler>(0.01);
	t_ass_ptr->assemble();

	// space-time
	auto st_op_ptr = make_shared<Space_time_assembler>(s_ass_ptr, t_ass_ptr, atoi(argv[1]));

	// set initial condition
	auto local_sizes = local_size(*(st_op_ptr->get_space_op()->get_mass()));
	auto time_bc = std::make_shared<UVector>(local_values(local_sizes.get(0), 0.));
	st_op_ptr->set_time_bc(time_bc);

	// set initial condition
	st_op_ptr->assemble();

	std::cout << "---------------------------------------------------" << std::endl;
	std::cout << "--------- space-time operator assembled -----------" << std::endl;
	std::cout << "---------------------------------------------------" << std::endl;

	// test MG set up
	std::vector<int> levels;
	for (int i = 2; i < argc; i++){
        levels.push_back(atoi(argv[i]));
    }

	Space_timeMG st_mg(init, st_op_ptr, levels);

	std::cout << "--------- space-time MG created -----------" << std::endl;

	// test solver
	// auto smoother = std::make_shared<GaussSeidel<USparseMatrix, UVector>>();
	auto smoother = std::make_shared<GMRES<USparseMatrix, UVector>>();
	// auto smoother = std::make_shared<ProjectedGaussSeidel<USparseMatrix, UVector, HOMEMADE>>();
//    auto linear_solver = std::make_shared<BiCGStab<USparseMatrix, UVector>>();
    auto linear_solver = std::make_shared<Factorization<USparseMatrix, UVector>>();
    st_mg.multigrid = std::make_shared<Space_timeMG::Multigrid>(smoother, linear_solver);

	st_mg.multigrid->max_it(30);
	st_mg.multigrid->mg_type(1);

	st_mg.init_mg<Elasticity_assembler>();
	st_mg.multigrid->update(st_op_ptr->get_space_time_system());

	std::cout << "--------- MG init done -----------" << std::endl;

	// initial guess
	auto s_ST     = local_size(*(st_op_ptr->get_space_time_system()));
	UVector x_0  = local_zeros(s_ST.get(0));

	st_mg.multigrid->verbose(true);
	st_mg.multigrid->describe(std::cout);

	// build space_time identity
	st_op_ptr->set_space_time_mass();
	st_mg.multigrid->apply(*(st_op_ptr->get_space_time_rhs()), x_0);

	std::cout << "--------- solve finshed -----------" << std::endl;

	// st_op_ptr.set_time_bc must be called
	impose_initial_condition(*st_op_ptr, x_0);

	//save_vec(raw_type(x_0), "solution.m", "sol");
	if(utopia::mpi_world_size() == 1)
		save_solution(*st_op_ptr, x_0); // works just in serial

	std::cout << "--------- solution saved -----------" << std::endl;

	return 0;
}
