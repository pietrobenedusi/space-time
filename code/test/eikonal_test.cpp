#include "Eikonal_assembler.hpp"
#include "Utilities.hpp"

using namespace libMesh;
using namespace utopia;
using std::make_shared;

int main(const int argc, char *argv[]) 
{

	libMesh::LibMeshInit init(argc, argv);

	// general settings	
	const bool two_d = false;
	// TODO put BC term!

	// non linear solver setting	
	int non_linear_iterations = 50;

	// model paramters	
	double tau = 3.;
	double c0 = 2.5;

	int boundary_tag = two_d ? 1 : 104;

    std::cout << "--------- parameters set -----------" << std::endl;

    auto mesh = make_shared<libMesh::DistributedMesh>(init.comm());
    
    if(two_d) {	    
	    mesh->read("../../mesh/rect2.1.e");
    } else {
    	//mesh->read("../../mesh/heart2.e");    	
    	mesh->read("../../mesh/HeartDimo_sidesets.e");   
    }
     
    std::cout << "--------- mesh read -----------" << std::endl;   

    Eikonal_assembler eikonal(mesh, boundary_tag, tau, c0);
    eikonal.set_diff_coeff(1.);
    eikonal.set_forcing_term(1.);

    std::cout << "--------- object created -----------" << std::endl;   
       
	eikonal.solve(0., non_linear_iterations);

	std::cout << "--------- solved -----------" << std::endl;   

	//output
	//Nemesis_IO out(*mesh);
	//out.write_timestep("../../output/eikonal.e", eikonal.get_equation_systems(), 1, 1);
	//out.write("../../output/eikonal.e", eikonal.get_equation_systems(), 1, 1);

	return 0;
}
