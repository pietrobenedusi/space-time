#include "Laplace_assembler.hpp"
#include "Space_assembler.hpp"
#include "Utilities.hpp"
#include "libmesh/mesh_refinement.h"
#include "utopia_ElementWisePseudoInverse.hpp"

using namespace libMesh;
using namespace utopia;
using std::make_shared;

int main(const int argc, char *argv[])
{
    libMesh::LibMeshInit init(argc, argv);

    utopia::Utopia::instance().set("disable-adaptivity", "true");
	
	// FHN paramters	
    const double u_min = -1., u_max = 1., u_unst = -0.5;
    // const double a = 5.0;
    // const double b = 0;  
    // const double c = 0.1;
    // const double d = 1./u_max;    

    const double a = 0.7;
    const double b = 0.8;  
    const double c = 12.5;
    // const double d = 1./u_max;
    

	const double diffusivity = 0.3;

	// time parameters
    const double T_fin = 100.;
    const int time_steps = 50000;
    const double dt = T_fin/(time_steps - 1);
    
    std::cout << "--------- Time parameters ----------" << std::endl;
    std::cout << "dt = " << dt << std::endl;
    std::cout << "T_fin = " << T_fin << std::endl;
    std::cout << "time_steps = " << time_steps << std::endl;

    double dt_critical = (2./(u_max - u_min))*(2./(u_max - u_min)/a);
    if (dt > dt_critical) {
        std::cout << "WARNING: dt may be too large for current reaction" << std::endl;
        std::cout << "set dt < " <<  c << std::endl;
    }

    std::cout << "--------- Read mesh -----------" << std::endl;

    auto mesh = make_shared<libMesh::DistributedMesh>(init.comm());

    // 1D
    // MeshTools::Generation::build_line(*mesh, 31, 0, 20, EDGE2);

    // 2D
    // mesh->read("../data/rect2.1.e");
    mesh->read("../data/gruviera.e");
    // mesh->read("../data/for_spiral_waves4.e");
    
    // 3D
    // mesh->read("../data/nice_heart.e" );
    // mesh->read("../data/cube_20x20x20.e");
    // mesh->read("../../data/heart2.e");
    // mesh->read("../../data/mesh_mc.e");
    
    std::cout << "--------- Assemble space operators -----------" << std::endl;

    // space
    Laplace_assembler s_ass(mesh, false);
    s_ass.set_diff_coeff(diffusivity);
    s_ass.assemble();

    // FEM matrices
    auto K = *s_ass.get_stiff();
    auto M = *s_ass.get_mass();

    // USparseMatrix inv_M;
    // read("../output/inv_M.m", inv_M);
    // auto inv_M_K = inv_M * K;

    UVector mass_vec = sum(M,1);
    e_pseudo_inv(mass_vec, mass_vec, 0.0000000001);
    
    // forcing factor
    UVector f;
    set_gaussian_forcing(s_ass, f, 15*u_max, 0., 1.0, -5.0, -5.0, 0.);
    
    
    auto size_loc = local_size(f).get(0);
    auto size_tot = local_size(f).get(0);

    // init variables
    UVector u = local_values(size_loc, u_min);
    UVector w = local_zeros(size_loc);

    UVector a_  = local_values(size_loc, a);
    UVector u_min_  = local_values(size_loc, u_min);
    UVector u_max_  = local_values(size_loc, u_max);
    UVector u_unst_ = local_values(size_loc, u_unst);

    // output file    
    Nemesis_IO out(*mesh);
    std::string path = "../output/monodomain.e";

    // write sol
    convert(u, *s_ass.get_equation_system().solution);
    s_ass.get_equation_system().solution->close();
    out.write_timestep(path, s_ass.get_equation_systems(), 1, dt);
    
    std::cout << "--------- Solve... -----------" << std::endl;

    int counter = 0;            

    for (int i = 0; i < time_steps; ++i)
    {        
        auto I_ion = a * e_mul(e_mul(u - u_min_, u - u_max_), (u - u_unst_)) + 0.8 * w;   

        double f_coeff = 0;
        if (i > 20000) f_coeff = 1.;        

        u = u - dt * (e_mul(K * u, mass_vec) + I_ion - e_mul(f, mass_vec));
        w = w + dt * (u + a_ - b * w)*(1./c);           

        // write sol
        if (i % 200 == 0)
        {
            std::cout << "time step = " << i << std::endl;

            convert(u, *s_ass.get_equation_system().solution);
            s_ass.get_equation_system().solution->close();    
            out.write_timestep(path, s_ass.get_equation_systems(), counter + 1, i * dt);
            counter++;
        }    
    }

    return 0;
}
