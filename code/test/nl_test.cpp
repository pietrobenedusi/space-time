#include "Space_timeMG.hpp"
#include "Laplace_assembler.hpp"
#include "Space_assembler.hpp"
#include "Time_trapezoidal_assembler.hpp"
#include "Eikonal_assembler.hpp"
#include "Space_time_FHN_Model.hpp"
#include "FHN_Model.hpp"
#include "Utilities.hpp"
#include "libmesh/mesh_refinement.h"

using namespace libMesh;
using namespace utopia;
using std::make_shared;


int main(const int argc, char *argv[])
{
    libMesh::LibMeshInit init(argc, argv);

    utopia::Utopia::instance().set("disable-adaptivity", "true");

	if (argc < 2)
	{
		std::cerr << "WARNING: correct usage: ./test time_steps" << std::endl;
		return 1;
	}

	// FHN paramters
	// double u_min = -85., u_max = 30., u_unst = -57.6, u_source = 0.;
    const double u_min = -1, u_max = 1., u_unst = -0.8;

   // const double alpha = -1.4e-3;
   // const double chi   = 400.;
   // const double diffusivity = 1.33/chi;

    const double alpha = -5.0;
	const double diffusivity = atof(argv[3]);

    std::cout << "gamma = " << alpha << std::endl;

	// time parameters
    const double T_fin = atof(argv[2]);
    const int time_steps = atoi(argv[1]) + 1;
    const double dt = T_fin/(time_steps - 1);
    const int q = 0;
    const int time_blocks = 10;

    std::cout << "--------- Time parameters ----------" << std::endl;

    std::cout << "dt = " << dt << std::endl;
    std::cout << "T_fin = " << T_fin << std::endl;
    std::cout << "time_steps = " << time_steps - 1 << std::endl;

    double c = (-2./(u_max - u_min))*(2./(u_max - u_min)/alpha);
    if (dt > c) {
        std::cout << "WARNING: dt may be too large for current reaction" << std::endl;
        std::cout << "set dt < " <<  c << std::endl;
    }

    std::cout << "--------- Read mesh -----------" << std::endl;

    auto mesh = make_shared<libMesh::DistributedMesh>(init.comm());

    // 1D
    // MeshTools::Generation::build_line(*mesh, 31, 0, 20, EDGE2);

    // 2D
    // mesh->read("../data/rect2.1.e");
    mesh->read("../data/gruviera.e");
    // int boundary_tag = 1;
    // double tau = 72.;

    // 3D
    // mesh->read("../data/nice_heart.e" );
    // mesh->read("../data/cube_20x20x20.e");
    // mesh->read("../../data/heart2.e");
    // mesh->read("../../data/mesh_mc.e");
    // mesh->read("../../data/HeartDimo_sidesets.e");
    // int boundary_tag = 105;
    // int boundary_tag = 5;

    std::cout << "--------- Assemble space and time operators -----------" << std::endl;

    // space
    Laplace_assembler s_ass(mesh, false);
    // s_ass.set_diff_coeff(diffusivity);
    s_ass.set_diff_tensor("../data/tensor_diffusion_cube_20.txt");
    s_ass.assemble();

    auto s_ass_ptr = make_shared<Laplace_assembler>(s_ass);

    // time
    // auto t_ass_ptr = make_shared<Time_trapezoidal_assembler>(dt);
    auto t_ass_ptr = make_shared<Time_slab_assembler>(dt, q + 1);
    t_ass_ptr->assemble();

    // space-time
    auto st_op_ptr = make_shared<Space_time_assembler>(s_ass_ptr, t_ass_ptr, time_steps);
    st_op_ptr->print_mu(diffusivity);

    // set time bc troug forcing factor
    UVector inital_forcing;
    set_gaussian_forcing(s_ass, inital_forcing, 4*u_max, u_min, 2., 0., 0., 0.);
    auto inital_forcing_ptr = make_shared<UVector>(inital_forcing);
    st_op_ptr->set_time_bc(inital_forcing_ptr);

    // st_op_ptr->set_time_bc(boundary_tag, u_source, u_min, s_ass.get_fem_space());
    // st_op_ptr->set_time_bc(u_source, false);

    st_op_ptr->assemble();

    std::cout << "Total #dofs = " << st_op_ptr->get_size_ST() << std::endl;

    mpi_world_barrier();
    std::cout << "--------- Space time operators assembled -----------" << std::endl;

    // const int levels = 4;
    // std::cout << "MG levels = " << levels << std::endl;

    // Space_timeMG st_mg(init, st_op_ptr, levels);
    // // st_mg.set_interpolation_flag(true);
    // // if (argc >= 4) st_mg.set_coarsening_time(atoi(argv[3]));
    // // st_mg.set_coarsening_time(2);
    // st_mg.init_mg<Laplace_assembler>();
    // st_mg.print_mu(diffusivity);
    // // st_mg.multigrid->verbose(true);

    // initial guess
    auto s_ST    = local_size(*(st_op_ptr->get_space_time_system()));
    UVector x_0  = local_values(s_ST.get(0), u_min);

    //    std::cout << "--------- Solve Eikonal -----------" << std::endl;

    //    double c0 = 1.;
    //    double tau = 5.; // for heart2 mesh

	// Eikonal_assembler eikonal(mesh, boundary_tag, tau, c0);
	// eikonal.solve();

	// eikonal_to_space_time(*st_op_ptr, *(eikonal.get_solution()), x_0, u_min, u_max);


    // output file
    // SpaceTimeIO io_e(*mesh, "../../output/eikonal.e");
    // io_e.write_time_block(*st_op_ptr, x_0);
    // return 0;

    SpaceTimeIO io(*mesh, "../output/monodomain.e");

    // monodomain
    Space_time_FHN_Model<LibMeshFunctionSpace, USparseMatrix, UVector> st_monodomain(st_op_ptr, s_ass.get_fem_space(), alpha, u_min, u_max, u_unst);

    std::cout << "--------- Monodomain set -----------" << std::endl;

    auto gmres = std::make_shared<GMRES<USparseMatrix, UVector>>();
    gmres->pc_type("bjacobi");

    Newton<USparseMatrix, UVector> nlsolver(gmres);
    nlsolver.verbose(true);
    if (abs(alpha) > 0.001) nlsolver.forcing_strategy(InexactNewtonForcingStartegies::SUPERLINEAR);


    for (int i = 0; i < time_blocks; ++i)
    {

        std::cout << std::endl << " Time block number " << i << std::endl << std::endl;

       // Newton<USparseMatrix, UVector> nlsolver(st_mg.multigrid);

        // auto LU = std::make_shared<Factorization<USparseMatrix, UVector>>();
        // auto gmres = std::make_shared<GMRES<USparseMatrix, UVector>>();
        // gmres->atol(1e-13);
        // gmres->rtol(1e-13);
        // gmres->stol(1e-13);
        // gmres->max_it(4000);



        // nlsolver.verbose(true);
        // nlsolver.atol(1e-13);
        // nlsolver.rtol(1e-13);
        // nlsolver.stol(1e-13);

        // if (abs(alpha) > 0.001) nlsolver.forcing_strategy(InexactNewtonForcingStartegies::QUADRATIC);
        // if (abs(alpha) > 0.001) nlsolver.forcing_strategy(InexactNewtonForcingStartegies::SUPERLINEAR);


        mpi_world_barrier();
        std::cout << "--------- Solve -----------" << std::endl;

        nlsolver.solve(st_monodomain, x_0);



        // auto subproblem = std::make_shared<utopia::Dogleg<USparseMatrix, UVector> >();
        // subproblem->atol(1e-12);
        // subproblem->rtol(1e-12);

        // TrustRegion<USparseMatrix, UVector> nlsolver(subproblem);
        // nlsolver.set_linear_solver(st_mg.multigrid);

        // TaoSolver<USparseMatrix, UVector> nlsolver;

        // backtracking: this sometime creates problems
        // auto strategy = std::make_shared<utopia::Backtracking<utopia::USparseMatrix, utopia::UVector>>();
        // nlsolver.set_line_search_strategy(strategy);

        // nlsolver.verbose(true);
        // nlsolver.solve(st_monodomain, x_0);

        // output file
        io.write_time_block(*st_op_ptr, x_0);

        // save_mat(raw_type(*st_op_ptr->get_space_time_system()), "../output/system.m", "ST");
        // save_vec(raw_type(x_0), "../output/x_0.m", "sol");

        if (i < time_blocks - 1)
        {

            // copy to next block // TODO questo bene
            auto final_time = std::make_shared<UVector>(local_zeros(local_size(*st_op_ptr->get_time_bc())));
            extract_final_time(*st_op_ptr, x_0, *final_time);

            // scale by mass matrix
            auto mass = st_op_ptr->get_space_op()->get_mass();

            *final_time = (*mass) * (*final_time);

            //set new initial condition
            st_op_ptr->set_time_bc(final_time, false);
            st_op_ptr->assemble_rhs();

            copy_solution_to_next_time_block(*st_op_ptr, *final_time, x_0);
        }
    }

    return 0;
}
