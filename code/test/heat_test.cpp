#include "Laplace_assembler.hpp"
#include "Time_trapezoidal_assembler.hpp"
#include "Space_timeMG.hpp"
#include "Utilities.hpp"

#include <chrono>  // for high_resolution_clock

using namespace utopia;
using namespace libMesh;
using std::make_shared;


int main(int argc, char *argv[])
{
	auto start = std::chrono::high_resolution_clock::now();

	libMesh::LibMeshInit init(argc, argv);

	utopia::Utopia::instance().set("disable-adaptivity", "true");

	std::cout << "+++++++++++++" << std::endl;
	std::cout << "MPI size = "<< mpi_world_size() << std::endl;
	std::cout << "+++++++++++++" << std::endl;

	const double D = 1.;
	// double f = 5.;
	const double T = 1.0;

	auto mesh = std::make_shared<DistributedMesh>(init.comm());
	// mesh->allow_renumbering(false);

	utopia::Chrono c;
	c.start();

	// super simple mesh for tests
	int Nx = 100;
	double l = 20;
	double dx = l/(Nx+1);

	// MeshTools::Generation::build_square(*mesh,
	// Nx, Nx,
	// -l, l,
	// -l, l,
	// //TET4);
	// QUAD4);


	// MeshTools::Generation::build_line(*mesh, Nx, 0, l, EDGE2);


    // mesh->read("../data/rect2.1.e");
    // mesh->read("../data/heart2-1.e");
    // mesh->read("../data/heart2-2.e");
    // mesh->read("../data/HeartDimo_sidesets.e");
    // mesh->read("../data/cube_test02.e");
    // mesh->read("../data/sphere-tet-6075.e");
    // mesh->read("../data/gruviera.e");
    // mesh->read("../data/cube_40x40x40.e");
    // mesh->read("../data/holes_fn_fn.e");
    mesh->read("../data/circle_cr.e");
     // mesh->read("../data/rings.e");

    std::cout << "--------- Mesh read -----------" << std::endl;

 //    int boundary_tag = 104;

    auto s_ass_ptr = make_shared<Laplace_assembler>(mesh, false);

    // vector diffusion 
	// s_ass_ptr->set_diff_coeff("../data/sample_const_diffusion_cube_20x20x20.txt");

	// scalar diffusion    
	s_ass_ptr->set_diff_coeff(D);

    // tensor diffusion
    // s_ass_ptr->set_diff_tensor("../data/tensor_diffusion_cube_20.txt");
	
	s_ass_ptr->assemble();

	// save_mat(raw_type(*s_ass_ptr->get_stiff()), "../output/Kcc.m", "Kc");
	// save_mat(raw_type(*s_ass_ptr->get_mass()), "../output/Mcc.m", "Mc");

	std::cout << "--------- space operator assembled -----------" << std::endl;

	// for (int i = 5; i < 6; ++i)
	// {
		// c.start();

		// double N_t = pow(2,i) + 1;
		// double N_t = 64 * mpi_world_size() + 1;
		double N_t = 1;
		double dt = T/N_t;

		std::cout << "==============================" << std::endl;
		std::cout << "======== dt = " << dt << std::endl;
		std::cout << "======== dx = " << dx << std::endl;
		std::cout << "======== N_t = " << N_t << " ============= "<< std::endl;
		std::cout << "==============================" << std::endl;

		// std::cout << "Total dofs =  " << 2 * N_t * Nx * Nx  << std::endl;
		std::cout << "mu =  " << D * dt / (dx * dx)  << std::endl;

		auto t_ass_ptr = make_shared<Time_slab_assembler>(dt,1);
		// auto t_ass_ptr = make_shared<Time_trapezoidal_assembler>(dt);
		t_ass_ptr->assemble();

		std::cout << "--------- time operator assembled -----------" << std::endl;

		// space time

		auto st_op_ptr = make_shared<Space_time_assembler>(s_ass_ptr, t_ass_ptr, N_t);
		st_op_ptr->print_mu(D);

		// bool set_compute_prec_ = false;
		// st_op_ptr->set_compute_prec(set_compute_prec_);

		// set time BC
		// // auto time_bc = std::make_shared<UVector>(zeros(st_op_ptr.get_size_A()));

		// bool initial_bc = false;
		// st_op_ptr->set_time_bc(0., initial_bc);

	    UVector inital_forcing;
	    set_gaussian_forcing(*s_ass_ptr, inital_forcing, 4, 0., 1.4, 0., 0., 0.);  // <------------ attenzione qui!
	    auto inital_forcing_ptr = make_shared<UVector>(inital_forcing);
    	st_op_ptr->set_time_bc(inital_forcing_ptr);

        // st_op_ptr->set_time_bc(boundary_tag, 10., 0., s_ass_ptr->get_fem_space());

		st_op_ptr->assemble();

		// save_mat(raw_type(*st_op_ptr->get_space_time_system()), "../output/C_c.m", "C");
		// save_vec(raw_type(*st_op_ptr->get_space_time_rhs()), "../output/rhs_c.m", "rhs");

		c.stop();
		utopia::mpi_world_barrier();
		if(utopia::mpi_world_rank() == 0)   std::cout << "Space-Time operator assembly: " << c << std::endl;

		mpi_world_barrier();
		std::cout << "--------- space-time operator assembled -----------" << std::endl;

		unsigned int levels;
		(argc >= 2) ? levels = atoi(argv[1]) : levels = 1;

		if (levels > 1)
		{
			// MG set up
			c.start();
			std::cout << levels << " MG levels" << std::endl;

			Space_timeMG st_mg(init, st_op_ptr, levels);
			st_mg.set_interpolation_flag(false);
			// st_mg.set_coarsening_space(1.7);
			st_mg.set_coarsening_time(1);

			// set spatial meshes manually
			// std::vector< std::shared_ptr<libMesh::DistributedMesh> > spatial_meshes;
			// spatial_meshes.reserve(levels - 1);
			
			// auto mesh_cr1 = std::make_shared<DistributedMesh>(init.comm());
			// // mesh_cr1->read("../data/holes_fn.e");
			// // mesh_cr1->read("../data/heart2-1.e");
			// mesh_cr1->read("../data/cylinder_fn.e");
			// spatial_meshes.push_back(mesh_cr1);
			
			// auto mesh_cr2 = std::make_shared<DistributedMesh>(init.comm());
			// mesh_cr2->read("../data/holes2.e");
			// mesh_cr2->read("../data/heart2.e");
			// mesh_cr2->read("../data/cylinder_cr.e");
			// spatial_meshes.push_back(mesh_cr2);

			// st_mg.set_spatial_meshes(spatial_meshes);

			if (argc >= 3) st_mg.set_coarsening_space(atof(argv[2]));
			if (argc >= 4) st_mg.set_smoothing_steps(atoi(argv[3]));

			std::cout << "--------- space-time MG created -----------" << std::endl;

			st_mg.init_mg<Laplace_assembler>();		// TODO con false non funziona
			st_mg.print_mu(D);

			std::cout << "--------- MG init done -----------" << std::endl;	

			c.stop();
			utopia::mpi_world_barrier();
			if(utopia::mpi_world_rank() == 0) 	std::cout << "MG init time: " << c << std::endl;


			// initial guess
			auto s_ST     = local_size(*(st_op_ptr->get_space_time_system()));
			UVector x_0  = local_zeros(s_ST.get(0));

			// solve
			c.start();
			st_mg.solve(x_0);
			c.stop();
			utopia::mpi_world_barrier();
			if(utopia::mpi_world_rank() == 0) 	std::cout << "Solve time: " << c << std::endl;

			// ierr = KSPDestroy(&solver);CHKERRQ(ierr);

			// SpaceTimeIO io(*mesh, "../output/heat.e");
	  // 		io.write_time_block(*st_op_ptr, x_0);

			// save_vec(raw_type(x_0), "../output/x_0.m", "sol");
		}
		else
		{
			auto s_ST     = local_size(*(st_op_ptr->get_space_time_system()));
			UVector x_0  = local_zeros(s_ST.get(0));

			// GMRES<USparseMatrix, UVector> gmres;
			// gmres.verbose(true);
			// gmres.max_it(3000);
			// gmres.pc_type("bjacobi");
			// gmres.describe(std::cout);

			// c.start();
			// gmres.update(st_op_ptr->get_space_time_system());
			// c.stop();

			// if(utopia::mpi_world_rank() == 0) 	std::cout << "GMRES set up time: " << c << std::endl;

			// c.start();
			// gmres.apply(*st_op_ptr->get_space_time_rhs(), x_0);
			// c.stop();

			// I do this because dunno how to get number of its with utopia
			c.start();
			PetscErrorCode ierr;
			KSP solver;
			ierr = KSPCreate(PETSC_COMM_WORLD,&solver);CHKERRQ(ierr);
			ierr = KSPSetType(solver,KSPGMRES);CHKERRQ(ierr);
			ierr = KSPSetOperators(solver, raw_type(*st_op_ptr->get_space_time_system()),
												 raw_type(*st_op_ptr->get_space_time_system()));CHKERRQ(ierr);

			ierr = KSPSetTolerances(solver,1e-9,1e-9,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
			ierr = KSPSetUp(solver);CHKERRQ(ierr);
			c.stop();

			if(utopia::mpi_world_rank() == 0) 	std::cout << "GMRES set up time: " << c << std::endl;

			c.start();
			ierr = KSPSolve(solver, raw_type(*st_op_ptr->get_space_time_rhs()), raw_type(x_0));CHKERRQ(ierr);
			c.stop();

			int its;
			KSPGetIterationNumber(solver,&its);
			std::cout << "GMRES iterations = " << its << std::endl;

			ierr = KSPDestroy(&solver);CHKERRQ(ierr);

			utopia::mpi_world_barrier();
			if(utopia::mpi_world_rank() == 0) 	std::cout << "GMRES solve time: " << c << std::endl;

			// SpaceTimeIO io(*mesh, "../output/heat.e");
	  // 		io.write_time_block(*st_op_ptr, x_0);

	  // 		save_vec(raw_type(x_0), "../output/x_0.m", "sol");
		}

		std::cout << "--------- solve finshed -----------" << std::endl;

		// UVector x_space = local_zeros(s_ass_ptr->get_size());
		// extract_nth_time(*st_op_ptr, x_0, x_space, 4, 0);
		// extract_nth_time(*st_op_ptr, x_0, x_space, 4, 0);

		// st_op_ptr.set_time_bc must be called
		//impose_initial_condition(*st_op_ptr, x_0);

		// std::cout << "--------- solution saved -----------" << std::endl;

		utopia::mpi_world_barrier();

		auto finish = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> elapsed = finish - start;
		std::cout << "Elapsed time: " << elapsed.count() << " s\n";
	// }

	return 0;
}
