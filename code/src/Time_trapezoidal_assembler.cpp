#include "Time_trapezoidal_assembler.hpp"

using namespace utopia;

void Time_trapezoidal_assembler::assemble(){

	// init matrices
	if(!mass)     { mass = std::make_shared<USparseMatrix>();    }
	if(!stiff)    { stiff = std::make_shared<USparseMatrix>();   }
	if(!coupling) { coupling = std::make_shared<USparseMatrix>();}

	// stiffness
	*stiff = values(1,1,1);

	// mass
	*mass = values(1,1,time_step/2.);

	// coupling
	*coupling = values(1,1,-1);
}