#include "Time_slab_assembler.hpp"

using namespace utopia;

//DG time assembler

void Time_slab_assembler::assemble(){

	// init matrices
	if(!mass)     { mass = std::make_shared<USparseMatrix>();    }
	if(!stiff)    { stiff = std::make_shared<USparseMatrix>();   }
	if(!coupling) { coupling = std::make_shared<USparseMatrix>();}

	const double half_time_step = time_step/2.;

	switch (size_time_slab) // switch on the degree of time elements
	{

		case 1:

			*stiff = values(1,1,1.);
			*mass =  values(1,1,half_time_step * 2.);
			*coupling = values(1,1,-1.);
		break;

		case 2:

			*stiff = zeros(2,2);
			{
				Write<USparseMatrix> w(*stiff);
				stiff->set(0,0, 1.125);
				stiff->set(0,1, 0.375);
				stiff->set(1,0,-1.125);
				stiff->set(1,1, 0.625);
			}

			*mass = zeros(2,2);
			{
				Write<USparseMatrix> w(*mass);
				mass->set(0,0,1.5);
				mass->set(1,1,0.5);
			}
			*mass = half_time_step * (*mass);

			*coupling = zeros(2,2);
			{
				Write<USparseMatrix> w(*coupling);
				coupling->set(0,1, 1.5);
				coupling->set(1,1,-0.5);
			}
			*coupling = - 1. * (*coupling);
		break;

		case 3:

			*stiff = zeros(3,3);
			{
				Write<USparseMatrix> w(*stiff);
				stiff->set(0,0, 1.213803846019419);
				stiff->set(0,1, 0.439578584621842);
				stiff->set(0,2,-0.095304225916338);
				stiff->set(1,0,-1.828467473510731);
				stiff->set(1,1, 0.397307265091693);
				stiff->set(1,2, 0.539748670360782);
				stiff->set(2,0, 0.614663627491312);
				stiff->set(2,1,-0.836885849713534);
				stiff->set(2,2, 0.555555555555556);
			}

			*mass = zeros(3,3);
			{
				Write<USparseMatrix> w(*mass);
				mass->set(0,0,0.752806125400935);
				mass->set(1,1,1.024971652376844);
				mass->set(2,2,0.222222222222222);
			}
			*mass = half_time_step * (*mass);

			*coupling = zeros(3,3);
			{
				Write<USparseMatrix> w(*coupling);
				coupling->set(0,2, 1.558078204724922);
				coupling->set(1,2,-0.891411538058256);
				coupling->set(2,2, 0.333333333333333);
			}
			*coupling = - 1. * (*coupling);
		break;

		case 4:

			*stiff = zeros(4,4);
			{
				Write<USparseMatrix> w(*stiff);

				stiff->set(0,0, 1.244312502452168);
				stiff->set(0,1, 0.424060667514088);
				stiff->set(0,2,-0.129169136276354);
				stiff->set(0,3, 0.038333606084292);

				stiff->set(1,0,-1.960072145410880);
				stiff->set(1,1, 0.474023056021126);
				stiff->set(1,2, 0.681155699740237);
				stiff->set(1,3,-0.168783205551504);

				stiff->set(2,0, 1.148477658986552);
				stiff->set(2,1,-1.310286077886972);
				stiff->set(2,2, 0.208747774860034);
				stiff->set(2,3, 0.599199599467212);

				stiff->set(3,0,-0.432718016027840);
				stiff->set(3,1, 0.412202354351759);
				stiff->set(3,2,-0.760734338323918);
				stiff->set(3,3, 0.53125);
			}

			*mass = zeros(4,4);
			{
				Write<USparseMatrix> w(*mass);
				mass->set(0,0,0.440924422353536);
				mass->set(1,1,0.776386937686344);
				mass->set(2,2,0.657688639960120);
				mass->set(3,3,0.125);
			}
			*mass = half_time_step * (*mass);

			*coupling = zeros(4,4);
			{
				Write<USparseMatrix> w(*coupling);
				coupling->set(0,3, 1.577537639774195);
				coupling->set(1,3,-0.973676595201020);
				coupling->set(2,3, 0.646138955426825);
				coupling->set(3,3,-0.250000000000000);
			}
			*coupling = - 1. * (*coupling);
		break;

		default:
			std::cerr << "ERROR: Order in time should be in [1,4]!" << std::endl;
			std::cerr << " order (q + 1) = " << size_time_slab << std::endl;
		break;
	}

	// old code
	// // stifness
	// *stiff = values(2,2,0.5);
	// {
	// 	Write<USparseMatrix> w(*stiff);
	// 	stiff->set(1,0,-0.5);
	// }

	// // mass
	// *mass = values(2,2,1./3.);
	// {
	// 	Write<USparseMatrix> w(*mass);
	// 	mass->set(1,0,1./6.);
	// 	mass->set(0,1,1./6.);
	// }
	// *mass = time_step * (*mass);

	// // coupling
	// *coupling = zeros(2,2);
	// {
	// 	Write<USparseMatrix> w(*coupling);
	// 	coupling->set(0,1,-1.);
	// }

	// size_time_slab = size(*mass).get(0);
}