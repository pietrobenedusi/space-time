#include "Space_time_assembler.hpp"
#include "Utilities.hpp"

using namespace utopia;
using std::make_shared;

void Space_time_assembler::init_sizes(){

	auto size_space = space_ops->get_size();
    auto size_time_slab = time_ops->get_size();

    size_A = size_time_slab * size_space;
    size_ST = time_steps * size_A;
}

void Space_time_assembler::set_time_bc(const int boundary_tag, const double u_in, const double u_out, const FE_space V){

    flag_time_bc = true;

    // FIXME this is a trick, could be improved using libmesh

    int dim = space_ops->get_mesh()->mesh_dimension();

    auto local_sizes = local_size(*(space_ops->get_mass()));
    time_bc = std::make_shared<UVector>(local_zeros(local_sizes.get(0)));

    auto u = trial(*V);
    auto v = test(*V);

    UVector weighted_bc;
    USparseMatrix mass_matrix;

    if(dim == 2) {
        auto initial_cond_surf = integral(inner(coeff(u_in), v), boundary_tag);
        auto mass_form = integral(inner(u, v), boundary_tag);

        utopia::assemble(initial_cond_surf, weighted_bc);
        utopia::assemble(mass_form, mass_matrix);
    } else if (dim == 3){
        auto initial_cond_surf = surface_integral(inner(coeff(u_in), v), boundary_tag);
        auto mass_form = surface_integral(inner(u, v), boundary_tag);

        utopia::assemble(initial_cond_surf, weighted_bc);
        utopia::assemble(mass_form, mass_matrix);
    } else {
        std::cerr << "ERROR: dimension = " << dim << " non supported" << std::endl;
    }

    UVector mass_vec = sum(mass_matrix, 1);
    UVector inv_mass_vec = local_zeros(local_size(mass_vec));

    {
        Read<UVector> r_m(mass_vec);
        Write<UVector> w_i(inv_mass_vec);
        auto r = range(mass_vec);

        for(auto i = r.begin(); i != r.end(); ++i) {
            auto val = mass_vec.get(i);

            if(std::abs(val) < 1e-15) {
                val = 1.;
            }

            inv_mass_vec.set(i, 1./val);
        }
    }

    *time_bc = e_mul(inv_mass_vec, weighted_bc);

    {
        ReadAndWrite<UVector> rw_t(*time_bc);
        auto r = range(*time_bc );
        for(auto i = r.begin(); i != r.end(); ++i) {
            if(std::abs(time_bc->get(i)) < 1e-15) {
                time_bc->set(i, u_out);
            }
        }
    }

    // scale by mass matrix
    *time_bc = (*(space_ops->get_mass())) * (*time_bc);  // Mh*f_0
}

void Space_time_assembler::set_time_bc(const double time_bc_, bool bc){

    flag_time_bc = true;

    auto local_sizes = local_size(*(space_ops->get_mass()));

    time_bc = std::make_shared<UVector>(local_values(local_sizes.get(0), time_bc_));

    // scale by mass matrix
    *time_bc = (*(space_ops->get_mass())) * (*time_bc);  // Mh*f_0

    if (bc) // apply space BC to the intial condition
    {
        apply_zero_boundary_conditions( space_ops->get_dof_map(), *time_bc);
        // TODO in general it is not zero...
    }
}

// Assemble one-slab blocks A and B
PetscErrorCode Space_time_assembler::assemble_slab_blocks(){

    // init block A and B
    if(!A) { A = std::make_shared<USparseMatrix>(); }
    if(!A_prec) { A_prec = std::make_shared<USparseMatrix>(); }
    if(!B) { B = std::make_shared<USparseMatrix>(); }

	PetscErrorCode ierr;

	ierr = kron(raw_type(*space_ops->get_stiff()), raw_type(*time_ops->get_mass()), raw_type(*A_prec));CHKERRQ(ierr);
	ierr = kron(raw_type(*space_ops->get_mass()), raw_type(*time_ops->get_stiff()), raw_type(*A));CHKERRQ(ierr);

	*A = *A + *A_prec;

	ierr = kron(raw_type(*space_ops->get_mass()), raw_type(*time_ops->get_coupling()), raw_type(*B));CHKERRQ(ierr);

    // TODO add stuff her for trap rule

    if (time_ops->get_coupling_need_stiffness_()) //trap condition here
    {
        *B = *B + *A_prec;
    }


	// Sanity checks // TODO put assert here or debug option
	int m_A, n_A, m_B, n_B;
    ierr = MatGetSize(raw_type(*A), &m_A, &n_A);CHKERRQ(ierr);
    ierr = MatGetSize(raw_type(*B), &m_B, &n_B);CHKERRQ(ierr);

    if (size_A != m_A)
    {
        std::cout << "WARNING: size_A != m_A!!" << std::endl;
        std::cout << "size_A = " << size_A << std::endl;
        std::cout << "m_A = " << m_A << std::endl;

        return -1;
    }

    if (m_A != m_B || n_A != n_B || n_A != m_B)
    {
        std::cerr << "WARNING: Matrices A and B does not have the same size!" << std::endl;
        return -1;
    }

    int m_A_local, m_B_local;
    ierr = MatGetLocalSize(raw_type(*A), &m_A_local, NULL);CHKERRQ(ierr);
    ierr = MatGetLocalSize(raw_type(*B), &m_B_local, NULL);CHKERRQ(ierr);

    if (m_A_local != m_B_local)
    {
        std::cerr << "WARNING: Matrices A and B does not have the same local size!" << std::endl;
        return -1;
    }

	return ierr;
}

void Space_time_assembler::assemble_rhs(UVector &rhs_space, UVector &st_rhs){

    // TODO: this is included in the next method, use a more general subroutine!

    // assert(bool(*rhs_space));

    // if(!(*st_rhs) { st_rhs = std::make_shared<UVector>(); }

    // auto size_time_slab = time_ops->get_size();

    // std::cout << "--------- qui ----------" << std::endl;

    // // local range
    // Range range_ = range(st_rhs); // TODO< is this fine?

    // std::vector<UVector::SizeType> index;
    // index.reserve(range_.extent());

    // std::cout << "--------- qui1 ----------" << std::endl;

    // //copy spatial rhs accordin to space-time indexing
    // for (int i = range_.begin(); i < range_.end(); i++)
    // {
    //     index.push_back((i % size_A)/size_time_slab);
    // }

    // std::cout << "--------- qui2 ----------" << std::endl;

    // // assign rhs
    // st_rhs = rhs_space.select(index);

    // std::cout << "--------- qui3 ----------" << std::endl;
}

// This is now the same for every time step
PetscErrorCode Space_time_assembler::assemble_rhs(){

    if(!space_time_rhs) { space_time_rhs = std::make_shared<UVector>(); }

    PetscErrorCode ierr = 0;

    auto size_time_slab = time_ops->get_size();
    auto rhs_space = space_ops->get_rhs();

    // local range
    Range rows_ = row_range(*space_time_system);

    std::vector<UVector::SizeType> index;
    index.reserve(rows_.extent());

    //copy spatial rhs according to space-time indexing
    for (int i = rows_.begin(); i < rows_.end(); i++)
    {
        index.push_back((i % size_A)/size_time_slab);
    }

    // assign rhs
    rhs_space->select(index, *space_time_rhs);

    // Scaling by the mass matrix

    // Make mass matrix sequential in PETSc
    auto mass_time_slab = time_ops->get_mass();

    PetscMPIInt    mpi_size;
    MPI_Comm_size(PETSC_COMM_WORLD,&mpi_size);

    Mat mass_time_slab_seq;
    Vec mass_sum;

    ierr = MatCreateRedundantMatrix(raw_type(*mass_time_slab), mpi_size, MPI_COMM_NULL, MAT_INITIAL_MATRIX, &mass_time_slab_seq);CHKERRQ(ierr);
    ierr = VecCreateSeq(PETSC_COMM_SELF, size_time_slab, &mass_sum);CHKERRQ(ierr);
    ierr = MatGetRowSum(mass_time_slab_seq, mass_sum);CHKERRQ(ierr);

    ierr = VecAssemblyBegin(mass_sum);CHKERRQ(ierr);
    ierr = VecAssemblyEnd(mass_sum);CHKERRQ(ierr);

    double mass_sum_elem;
    int zero_ = 0;

    // TODO this is ok if all the rows of M have the same sum
    ierr = VecGetValues(mass_sum, 1, &zero_, &mass_sum_elem);CHKERRQ(ierr);

    (*space_time_rhs) *= mass_sum_elem;

    // clean
    ierr = MatDestroy(&mass_time_slab_seq);CHKERRQ(ierr);
    ierr = VecDestroy(&mass_sum);CHKERRQ(ierr);

    // Small note:
    // (Mh kron Mt) * (f_s kron [1 ; 1]) is equal too
    // ((Mh*f_s) kron Mt ) * [1 ; 1] --> Mh*f_s is rhs_space

    // For BC in time (Mh kron [1 0 ; 0 0]) * (f_0 kron [1 ; 0]) =
    // = ((Mh*f_0) kron [1 0 ; 0 0] ) * [1 ; 0] = [(Mh*f_0)(0) 0 (Mh*f_0)(1) 0 ... (Mh*f_0)(N) 0]

    if (time_bc){ //TODO problema qui

        mpi_world_barrier();

        if(mpi_world_size() == 1) {

            Read<UVector> r(*time_bc);
            Write<UVector> w(*space_time_rhs);

            for (auto i = 0; i < size_A; i++) {
                if (i % size_time_slab == 0) {
                    auto index0 = i/size_time_slab;
                    space_time_rhs->set(i, time_bc->get(index0) );
                }
            }
        } else {

            // make *time_bc accesible from all procs
            Vec            rhs_t_seq;
            VecScatter     ctx;

            ierr = VecScatterCreateToAll(raw_type(*time_bc), &ctx, &rhs_t_seq);CHKERRQ(ierr); // TODO not to all?
            ierr = VecScatterBegin(ctx, raw_type(*time_bc), rhs_t_seq,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
            ierr = VecScatterEnd(ctx, raw_type(*time_bc), rhs_t_seq,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
            ierr = VecScatterDestroy(&ctx);CHKERRQ(ierr);

            ierr = VecAssemblyBegin(rhs_t_seq);CHKERRQ(ierr);
            ierr = VecAssemblyEnd(rhs_t_seq);CHKERRQ(ierr);

            {
                Write<UVector> w(*space_time_rhs);

                // just on the first node in the first block in time
                for (int i = rows_.begin(); i < size_A && i < rows_.end(); i++){

                    if ( i % size_time_slab == 0){

                        double v;
                        int index0 = i/size_time_slab;
                        ierr = VecGetValues(rhs_t_seq, 1, &index0, &v);CHKERRQ(ierr);

                        space_time_rhs->set(i,  v);
                    }
                }
            }

            ierr = VecDestroy(&rhs_t_seq);CHKERRQ(ierr);
        }
    }

    return ierr;
}


PetscErrorCode Space_time_assembler::assemble_preconditioner()
{
    PetscErrorCode ierr;

    // if no preconditionere is needed
    if(!compute_prec_) {

        ierr = MatDestroy(&raw_type(*A_prec));CHKERRQ(ierr);
        return 0.;
    }

    std::cout << "begin: assemble_preconditioner" << std::endl;

    // init
    if(!space_time_system_prec) { space_time_system_prec = std::make_shared<USparseMatrix>(); }

    // prealloc
    int total_nz = count_max_nz(raw_type(*A_prec));

    // This it the Precondioner
    ierr = MatCreate(PETSC_COMM_WORLD, &raw_type(*space_time_system_prec));CHKERRQ(ierr);
    ierr = MatSetSizes(raw_type(*space_time_system_prec),PETSC_DECIDE, PETSC_DECIDE, size_ST, size_ST);CHKERRQ(ierr); // TODO maybe better choose by yourself the size
    ierr = MatSetType(raw_type(*space_time_system_prec),MATAIJ);CHKERRQ(ierr);
    ierr = MatSeqAIJSetPreallocation(raw_type(*space_time_system_prec), total_nz, NULL);CHKERRQ(ierr);
    ierr = MatMPIAIJSetPreallocation(raw_type(*space_time_system_prec), total_nz, NULL, total_nz, NULL);CHKERRQ(ierr);

    //MatSetOption(raw_type(*space_time_system_prec), MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);

    // Filling the space-time matrix
    int start, end;
    ierr = MatGetOwnershipRange(raw_type(*space_time_system_prec), &start, &end);CHKERRQ(ierr);

    // copy the necessary block of A and B
    int start_A, end_A, block_rows;

    if ((end - start) > size_A) // if the block is bigger then A you get the whole A
    {
        block_rows = size_A;
        start_A = 0;
    }
    else
    {
        start_A = start % size_A;
        end_A = end % size_A;

        if (start_A > end_A) // if the block is wird you get the whole A
        {
            block_rows = size_A;
            start_A = 0;
        }
        else{
            block_rows = end_A - start_A;
        }
    }

    assert(block_rows > 0 && "Space time block has negative length.");

    // get the block
    Mat *submat_A;
    IS   is_row, is_col;

    ierr = ISCreateStride(PETSC_COMM_SELF, block_rows, start_A, 1, &is_row);CHKERRQ(ierr);
    ierr = ISCreateStride(PETSC_COMM_SELF, size_A, 0, 1, &is_col);CHKERRQ(ierr);

    ierr = MatGetSubMatrices(raw_type(*A_prec), 1, &is_row, &is_col, MAT_INITIAL_MATRIX, &submat_A);CHKERRQ(ierr);

    // assemble space-time matrix
    PetscInt ncols_A;
    const PetscInt    *cols_A;
    const PetscScalar *vals_A;

    for (int i = start; i < end; ++i)
    {
        int i_cyclic = i % block_rows;

        // set A
        ierr = MatGetRow(submat_A[0],i_cyclic,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);

        for (int j = 0; j < ncols_A; ++j)
        {
            int col_new = size_A*(i/size_A) + cols_A[j];

            ierr = MatSetValues(raw_type(*space_time_system_prec),1,&i,1,&col_new,&vals_A[j],INSERT_VALUES);CHKERRQ(ierr);
        }

        ierr = MatRestoreRow(submat_A[0],i_cyclic,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);
    }


    // clean
    ierr = MatDestroy(&submat_A[0]);CHKERRQ(ierr);
    ierr = PetscFree(submat_A);CHKERRQ(ierr);
    ierr = ISDestroy(&is_row);CHKERRQ(ierr);
    ierr = ISDestroy(&is_col);CHKERRQ(ierr);

    ierr = MatAssemblyBegin(raw_type(*space_time_system_prec),MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(raw_type(*space_time_system_prec),MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

    std::cout << "end: assemble_preconditioner" << std::endl;
    optimize_nnz(*space_time_system_prec);
    return ierr;
}

PetscErrorCode Space_time_assembler::assemble(){

    // check if space and time ops have been assembled
    assert(space_ops->get_mass() && "Assemble space operators!");
    assert(time_ops->get_mass() && "Assemble time operators!");

    assembled_flag_ = true;

    //utopia::Chrono c;
    //c.start();

    if (!flag_time_bc)
    {
        std::cerr << "WARNING: initial time bc not set!" << std::endl;
        std::cerr << "Call set_time_bc()..." << std::endl;
    }

	assemble_slab_blocks();

    // init space-time matrix
    if(!space_time_system) { space_time_system = std::make_shared<USparseMatrix>(); }

	PetscErrorCode ierr;

    //Preallocation of memory
    int nz_A = count_max_nz(raw_type(*A));
    int nz_B = count_max_nz(raw_type(*B));

    int total_nz = nz_A + nz_B;

    // organize partioition of space time matrix dependig on A and MPI size
    int block_size = (int) ceil(((double) size_ST)/mpi_world_size());
    bool distribute_flag = block_size < size_A;

    std::vector<int> local_sizes;
    int local_size = 0;

    if (distribute_flag)
    {
        local_sizes = generate_distribution(size_A, size_ST);
        local_size = local_sizes[mpi_world_rank()];

        // check for testing
        int tot_size = 0;
        for (int i = 0; i < mpi_world_size(); ++i){ tot_size += local_sizes[i]; }

        if (tot_size != size_ST && mpi_world_rank() == 0)
        {
            std::cout << "WANRING: generate_distribution() has created incosistent local sizes!" << std::endl;
        }
    }

    // create the matrix trying to use an even partiotion
    ierr = MatCreate(PETSC_COMM_WORLD,&raw_type(*space_time_system));CHKERRQ(ierr);

    if (distribute_flag) // if the block size is smaller then size_A
    {
        ierr = MatSetSizes(raw_type(*space_time_system), local_size, local_size, size_ST, size_ST);CHKERRQ(ierr);
    }
    else
    {
        ierr = MatSetSizes(raw_type(*space_time_system),PETSC_DECIDE, PETSC_DECIDE, size_ST, size_ST);CHKERRQ(ierr);
    }

    ierr = MatSetType(raw_type(*space_time_system),MATAIJ);CHKERRQ(ierr);

    ierr = MatSeqAIJSetPreallocation(raw_type(*space_time_system), total_nz, NULL);CHKERRQ(ierr);
    ierr = MatMPIAIJSetPreallocation(raw_type(*space_time_system), total_nz, NULL, total_nz, NULL);CHKERRQ(ierr);

    // Filling the space-time matrix
    int start, end;
    ierr = MatGetOwnershipRange(raw_type(*space_time_system), &start, &end);CHKERRQ(ierr);

    // copy the necessary block of A and B
    int start_A = 0;
    int block_rows;

    // take all block A
    if (!distribute_flag)
    {
        block_rows = size_A;
    }
    else
    {
        for (int i = 0; i < mpi_world_rank(); ++i)
        {
            start_A += local_sizes[i];
        }

        start_A = start_A % size_A;
        block_rows = local_size;
    }

    // get the block
    Mat *submat_A, *submat_B;
    IS   is_row, is_col;

    ierr = ISCreateStride(PETSC_COMM_SELF, block_rows, start_A, 1, &is_row);CHKERRQ(ierr);
    ierr = ISCreateStride(PETSC_COMM_SELF, size_A, 0, 1, &is_col);CHKERRQ(ierr);

    ierr = MatGetSubMatrices(raw_type(*A), 1, &is_row, &is_col, MAT_INITIAL_MATRIX, &submat_A);CHKERRQ(ierr);
    ierr = MatGetSubMatrices(raw_type(*B), 1, &is_row, &is_col, MAT_INITIAL_MATRIX, &submat_B);CHKERRQ(ierr);

    // assemble space-time matrix
    PetscInt ncols_A , ncols_B;
    const PetscInt    *cols_A, *cols_B;
    const PetscScalar *vals_A, *vals_B;

    int counter = 0;

    for (int i = start; i < end; ++i)
    {
        int i_cyclic;

        if (distribute_flag)
        {
            i_cyclic = counter % size_A;
        }
        else
        {
            i_cyclic = i % size_A;
        }

        // set A
        ierr = MatGetRow(submat_A[0],i_cyclic,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);

        for (int j = 0; j < ncols_A; ++j)
        {
            int col_new = size_A*(i/size_A) + cols_A[j];

            ierr = MatSetValues(raw_type(*space_time_system),1,&i,1,&col_new,&vals_A[j],INSERT_VALUES);CHKERRQ(ierr);
        }

        ierr = MatRestoreRow(submat_A[0],i_cyclic,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);


        // set B
        if (i >= size_A )
        {
            ierr = MatGetRow(submat_B[0],i_cyclic,&ncols_B,&cols_B,&vals_B);CHKERRQ(ierr);

            for (int j = 0; j < ncols_B; ++j)
            {
                int col_new = size_A*(i/size_A - 1) + cols_B[j];

                ierr = MatSetValues(raw_type(*space_time_system),1,&i,1,&col_new,&vals_B[j],INSERT_VALUES);CHKERRQ(ierr);
            }

            ierr = MatRestoreRow(submat_B[0],i_cyclic,&ncols_B,&cols_B,&vals_B);CHKERRQ(ierr);
        }

        counter++;
    }

    // clean
    ierr = MatDestroy(&submat_A[0]);CHKERRQ(ierr);
    ierr = PetscFree(submat_A);CHKERRQ(ierr);
    ierr = MatDestroy(&submat_B[0]);CHKERRQ(ierr);
    ierr = PetscFree(submat_B);CHKERRQ(ierr);
    ierr = ISDestroy(&is_row);CHKERRQ(ierr);
    ierr = ISDestroy(&is_col);CHKERRQ(ierr);

    // PETSc assemble
    ierr = MatAssemblyBegin(raw_type(*space_time_system),MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(raw_type(*space_time_system),MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

    optimize_nnz(*space_time_system);

    // A nad B are freed here!
    ierr = MatDestroy(&raw_type(*A));CHKERRQ(ierr);
    ierr = MatDestroy(&raw_type(*B));CHKERRQ(ierr);

    assemble_rhs();
    assemble_preconditioner();

    return ierr;
}

// I_t kron M_t kron M_s
PetscErrorCode Space_time_assembler::set_space_time_mass(bool implicit_flag){

    PetscErrorCode ierr;

    if(!Mass) { Mass = std::make_shared<USparseMatrix>(); }

    USparseMatrix Mass_slab;
    ierr = kron(raw_type(*space_ops->get_mass()), raw_type(*time_ops->get_mass()), raw_type(Mass_slab));CHKERRQ(ierr);

    if (implicit_flag)
    {
        std::cout << "IMPLICIT mass is used" << std::endl;

        USparseMatrix time_id = identity(time_steps,time_steps);
        
        ierr = kron(raw_type(time_id), raw_type(Mass_slab), raw_type(*Mass), &raw_type(*space_time_system));CHKERRQ(ierr);
    }
    else // L_t kron M_t kron M_s
    {
        std::cout << "EXPLICIT mass is used" << std::endl;

        USparseMatrix time_shifted_id = zeros(time_steps,time_steps);

        // fill shifted matrix
        Range rows_ = row_range(time_shifted_id); 
        {
            Write<USparseMatrix> w(time_shifted_id);
            
            for (int i = rows_.begin(); i < rows_.end(); i++) 
            {                    
                if (i > 0) time_shifted_id.set(i, i - 1 , 1.0);                                   
            }
        }
        
        ierr = kron(raw_type(time_shifted_id), raw_type(Mass_slab), raw_type(*Mass), &raw_type(*space_time_system));CHKERRQ(ierr);
    }
    
    return ierr;
}

// I_t kron M_t kron I_s
PetscErrorCode Space_time_assembler::set_time_slabs_mass(){

    PetscErrorCode ierr;

    if(!time_slabs_mass) { time_slabs_mass = std::make_shared<USparseMatrix>(); }

    USparseMatrix time_id  = identity(time_steps,time_steps);
    USparseMatrix space_id = identity(space_ops->get_mass()->size());

    Mat Mass_slab;

    ierr = kron(raw_type(space_id), raw_type(*time_ops->get_mass()), Mass_slab);CHKERRQ(ierr);
    ierr = kron(raw_type(time_id), Mass_slab, raw_type(*time_slabs_mass));CHKERRQ(ierr);

    return ierr;
}


void Space_time_assembler::print_mu(double diffusivity) const {

    if(utopia::mpi_world_rank() == 0) {

        double mu = 0.0;
        int counter = 0;
        double dx = 0.;

        auto mesh = space_ops->get_mesh();

        for(auto e_it = mesh->local_elements_begin();
                e_it != mesh->local_elements_end();
                ++e_it) {

            dx += ((**e_it).hmax() + (**e_it).hmin())/2.;

            counter++;
        }

        dx = dx/counter;

        auto dt = time_ops->get_time_step();
        mu = diffusivity * dt / (dx * dx);

        std::cout << "" << std::endl;
        std::cout << ".---------------" << std::endl;
        std::cout << "| mu = " << mu <<  std::endl;
        std::cout << "'---------------" << std::endl;
        std::cout << "" << std::endl;

        if (mu > 10 || mu < 0.1)  std::cout << "WARNING: 0.1 < mu < 10 for better MG convergence!" << std::endl;
    }
}


