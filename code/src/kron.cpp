#include <iostream>
#include "kron.hpp"


// new kron, more scalable and optimized for memory
PetscErrorCode kron(const Mat &A, const Mat &B, Mat &AB, Mat* AB_structure) { 

    // A distributed, B local, AB distributed    
    PetscErrorCode ierr;

    // input matrices size informations
    PetscInt m_A, n_A, m_B, n_B, start, end; // mxn matrices
    
    ierr = MatGetSize(A, &m_A, &n_A);CHKERRQ(ierr);
    ierr = MatGetSize(B, &m_B, &n_B);CHKERRQ(ierr);

    const bool AB_square = (m_A*m_B == n_A*n_B);

    ierr = MatGetOwnershipRange(A, &start, &end);CHKERRQ(ierr);

    PetscInt ncols_A, ncols_B;
    const PetscInt    *cols_A, *cols_B;
    const PetscScalar *vals_A, *vals_B; 

    int nz_A = count_max_nz(A);
    int nz_B = count_max_nz(B);
    
    int max_nz_AB = 1 + nz_A * nz_B; // this is not 100% safe if the local size change, m_A_local would solve this

    ierr = MatCreate(PETSC_COMM_WORLD,&AB);CHKERRQ(ierr); 
    if (!AB_structure)
    {    
        ierr = MatSetSizes(AB,PETSC_DECIDE, PETSC_DECIDE, m_A*m_B, n_A*n_B);CHKERRQ(ierr);  
    }
    else{

        PetscInt m_aux, n_aux;
        ierr = MatGetLocalSize(*AB_structure, &m_aux,  &n_aux);CHKERRQ(ierr);

        if (AB_square) // TODO: now just for AB_structure from square
        {
            ierr = MatSetSizes(AB, m_aux, n_aux, m_A*m_B, n_A*n_B);CHKERRQ(ierr);  
        }
        else{
            ierr = MatSetSizes(AB, m_aux, PETSC_DECIDE, m_A*m_B, n_A*n_B);CHKERRQ(ierr);     
        }
    }
    
    ierr = MatSetType(AB,MATAIJ);CHKERRQ(ierr); 
    
    ierr = MatSeqAIJSetPreallocation(AB, max_nz_AB, 0);CHKERRQ(ierr);
    ierr = MatMPIAIJSetPreallocation(AB, max_nz_AB, 0, max_nz_AB, NULL);CHKERRQ(ierr);  

    // -------------------------------------------------------------

    // compute local size
    int start_AB, end_AB;
    ierr = MatGetOwnershipRange(AB, &start_AB, &end_AB);CHKERRQ(ierr);
    const int local_size = end_AB - start_AB;

    // copy the part of A that is needed 
    const int start_A = start_AB/m_B;
    const int end_A = end_AB/m_B;

    int block_rows_A = end_A - start_A + 1;
    
    if(start_A + block_rows_A > m_A)  block_rows_A-- ; 
    
    Mat  *submat_A;    
    IS   is_row, is_col;
    
    ierr = ISCreateStride(PETSC_COMM_SELF, block_rows_A, start_A, 1, &is_row);CHKERRQ(ierr); 
    ierr = ISCreateStride(PETSC_COMM_SELF, n_A, 0, 1, &is_col);CHKERRQ(ierr); 
    
    ierr = MatGetSubMatrices(A, 1, &is_row, &is_col, MAT_INITIAL_MATRIX, &submat_A);CHKERRQ(ierr);  

    // copy the necessary block of B
    PetscMPIInt    mpi_size;
    MPI_Comm_size(PETSC_COMM_WORLD,&mpi_size);            

    // check how much of B we need to use 
    MatType type;
    MatGetType(B, &type);
    
    bool use_entire_B = (mpi_size == 1) || (strncmp(type,"seq",3) == 0) || (local_size > m_B);

    int start_B = 0;
    int end_B;
    int block_rows_B;
    
    // take all block B
    if (use_entire_B) 
    {
        block_rows_B = m_B;        
    }
    else
    {
        start_B = start_AB % m_B; 
        end_B = end_AB % m_B;    
            
        if (start_B > end_B) // if the block is weird you get the whole B
        {
            start_B = 0;
            block_rows_B = m_B;            
        }
        else{
            block_rows_B = end_B - start_B;
        }             
    }

    if (block_rows_B == 0) block_rows_B = m_B;
    
    // get the block  
    Mat  *submat_B;
    
    ierr = ISCreateStride(PETSC_COMM_SELF, block_rows_B, start_B, 1, &is_row);CHKERRQ(ierr); 
    ierr = ISCreateStride(PETSC_COMM_SELF, n_B, 0, 1, &is_col);CHKERRQ(ierr); 
    
    ierr = MatGetSubMatrices(B, 1, &is_row, &is_col, MAT_INITIAL_MATRIX, &submat_B);CHKERRQ(ierr);  

    // kron computations 
    for (int i = start_AB; i < end_AB; i++) { 


        int i_A = i/m_B - start_A;
        int i_B = (i % m_B) - start_B;      

        ierr = MatGetRow(submat_A[0],i_A,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);
        ierr = MatGetRow(submat_B[0],i_B,&ncols_B,&cols_B,&vals_B);CHKERRQ(ierr);

        for (int j = 0; j < ncols_A; ++j){    // loop over A non-zero elements in the row i_A          
            for (int l = 0; l < ncols_B; ++l) // loop over B non-zero elements in the row i_B
            {                                                                                
                const PetscInt p_col = cols_A[j]*n_B + cols_B[l];                           
                const PetscScalar val = vals_A[j]*vals_B[l];   

                ierr = MatSetValues(AB,1,&i,1,&p_col,&val,INSERT_VALUES);CHKERRQ(ierr); 
            }
        }

        ierr = MatRestoreRow(submat_A[0],i_A,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);
        ierr = MatRestoreRow(submat_B[0],i_B,&ncols_B,&cols_B,&vals_B);CHKERRQ(ierr);            
    }
    
    ierr = MatAssemblyBegin(AB,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(AB,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

    ierr = MatDestroy(&submat_B[0]);CHKERRQ(ierr);
    ierr = MatDestroy(&submat_A[0]);CHKERRQ(ierr);

    return ierr;
}

// old kron
// PetscErrorCode kron(const Mat &A, const Mat &B_in, Mat &AB) {

//     // A distributed, B local, AB distributed    
//     PetscErrorCode ierr;

//     PetscMPIInt    mpi_size;
//     MPI_Comm_size(PETSC_COMM_WORLD,&mpi_size);

//     // create sequantial copy of the second matrix if necessary
//     MatType type;
//     MatGetType(B_in, &type);
//     Mat B;

//     // B is copied if distributed
//     bool b_is_a_copy = mpi_size > 1 && strncmp(type,"mpi",3) == 0;
   
//     if (b_is_a_copy) 
//     {
//         ierr = MatCreateRedundantMatrix(B_in,mpi_size,PETSC_COMM_SELF,MAT_INITIAL_MATRIX, &B);CHKERRQ(ierr);
//     } else {
//         B = B_in;
//     }
    
//     // input matrices size informations
//     PetscInt m_A, n_A, m_A_local, n_A_local, m_B, n_B, m_B_local, n_B_local, start, end; // mxn matrices
    
//     ierr = MatGetLocalSize(A, &m_A_local, &n_A_local);CHKERRQ(ierr);
//     ierr = MatGetSize(A, &m_A, &n_A);CHKERRQ(ierr);

//     ierr = MatGetLocalSize(B, &m_B_local, &n_B_local);CHKERRQ(ierr);
//     ierr = MatGetSize(B, &m_B, &n_B);CHKERRQ(ierr);

//     ierr = MatGetOwnershipRange(A, &start, &end);CHKERRQ(ierr);

//     PetscInt ncols_A, ncols_B;
//     const PetscInt    *cols_A, *cols_B;
//     const PetscScalar *vals_A, *vals_B; 

//     // preallocation
//     // MatInfo           matinfo_A, matinfo_B;
    
//     // MatGetInfo(A,MAT_LOCAL,&matinfo_A); 
//     // MatGetInfo(B,MAT_LOCAL,&matinfo_B); 

//     // int nz_A = ceil(matinfo_A.nz_used/(m_A_local == 0 ? 1 : m_A_local));
//     // int nz_B = ceil(matinfo_B.nz_used/(m_B_local == 0 ? 1 : m_B_local));

//     int nz_A = count_max_nz(A);
//     int nz_B = count_max_nz(B);

//     int max_nz_AB = 1 + nz_A * nz_B; // this is not 100% safe if the local size change, m_A_local would sovle thism

//     ierr = MatCreate(PETSC_COMM_WORLD,&AB);CHKERRQ(ierr); 
//     //ierr = MatSetSizes(AB,m_A_local*m_B, n_A_local*n_B, m_A*m_B, n_A*n_B);CHKERRQ(ierr); // this will cause problems with utopia mugltrid, even if more reliable TODO
//     ierr = MatSetSizes(AB,PETSC_DECIDE, PETSC_DECIDE, m_A*m_B, n_A*n_B);CHKERRQ(ierr);  
//     ierr = MatSetType(AB,MATAIJ);CHKERRQ(ierr); 
    
//     ierr = MatSeqAIJSetPreallocation(AB, max_nz_AB, 0);CHKERRQ(ierr);
//     ierr = MatMPIAIJSetPreallocation(AB, max_nz_AB, 0, max_nz_AB, NULL);CHKERRQ(ierr);  // TODO, can be improved 

//     // -------------------------------------------------------------
	
// 	//MatSetOption(AB, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);

//     // kron computations 
//     for (int i = start; i < end; i++) { 

//         ierr = MatGetRow(A,i,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);

//         for (int j = 0; j < ncols_A; ++j) // loop over A non-zero elements in the row i
//         {
//             for (int k = 0; k < m_B; ++k) // loop over B rows
//             {
//                 ierr = MatGetRow(B,k,&ncols_B,&cols_B,&vals_B);CHKERRQ(ierr);

//                 for (int l = 0; l < ncols_B; ++l) // loop over B non-zero elements in the row k
//                 {                                            
//                     const PetscInt p_row = i*m_B + k;
//                     const PetscInt p_col = cols_A[j]*n_B + cols_B[l];           
                    
//                     const PetscScalar val = vals_A[j]*vals_B[l];                                                                                        

//                     ierr = MatSetValues(AB,1,&p_row,1,&p_col,&val,INSERT_VALUES);CHKERRQ(ierr); 
//                 }
//                 ierr = MatRestoreRow(B,i,&ncols_B,&cols_B,&vals_B);CHKERRQ(ierr);
//             }
//         }
//         ierr = MatRestoreRow(A,i,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);
//     }
    
//     ierr = MatAssemblyBegin(AB,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
//     ierr = MatAssemblyEnd(AB,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

//     if(b_is_a_copy) {
//         ierr = MatDestroy(&B);CHKERRQ(ierr); 
//     }

//     return ierr;
// }



