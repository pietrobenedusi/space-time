#include "Space_timeMG.hpp"
#include "libmesh/mesh_refinement.h"
#include "libmesh/mesh_tools.h"
#include "moonolith_communicator.hpp"
#include "utopia_assemble_volume_transfer.hpp"
#include "utopia_petsc_Vector_impl.hpp" // per transform_values()

#include <assert.h>
#include <math.h>       /* cbrt */

using namespace libMesh;
using namespace utopia;
using std::make_shared;
using std::vector;

// Space_timeMG::Space_timeMG(const LibMeshInit &init_, const std::shared_ptr<Space_time_assembler> space_time_op_, const unsigned int number_of_levels_) :
// init(&init_), space_time_op(space_time_op_), number_of_levels(number_of_levels_){

//     if (!space_time_op_->get_assembled_flag()) std::cout << "WARNING: Space-Time object has not been assembled!" << std::endl;
// }


Space_timeMG::Space_timeMG(libMesh::Parallel::Communicator &comm, const std::shared_ptr<Space_time_assembler> space_time_op_, const unsigned int number_of_levels_) :
comm(&comm), space_time_op(space_time_op_), number_of_levels(number_of_levels_){

    if (!space_time_op_->get_assembled_flag()) std::cout << "WARNING: Space-Time object has not been assembled!" << std::endl;
}

void Space_timeMG::set_spatial_meshes(){  // refine box uniformely! -> nested meshes?

    spatial_meshes.reserve(number_of_levels - 1);

    // Init finer mesh
    auto space_op   = space_time_op->get_space_op();
    auto space_size = space_op->get_size();
    const auto dim  = space_op->get_mesh()->mesh_dimension();

    if (coarsening_space == 1) // use the fine mesh in coarse spaces
    {
        for (unsigned int i = 0; i < number_of_levels - 1; ++i){

            spatial_meshes.push_back(space_op->get_mesh());            
        }
    }
    else
    {    
        // for dim = 1 interpolation not supported (because equivalent to L2 proj)
        if (dim == 1) use_interpolation_ = false;

        auto bounding_box = MeshTools::create_bounding_box(*space_op->get_mesh());
        auto box_min = bounding_box.min();
        auto box_max = bounding_box.max();

        // scaling factors for element per side, if the domain is not a cube
        double mean_box = 0;

        for (unsigned int i = 0; i < dim; ++i)
        {
            mean_box += box_max(i) - box_min(i);
        }
        mean_box = mean_box/dim;

        double scaling_0 = ((box_max(0) - box_min(0))/mean_box);
        double scaling_1 = ((box_max(1) - box_min(1))/mean_box);
        double scaling_2 = ((box_max(2) - box_min(2))/mean_box);

        // to compute the number of elements for coarse meshes
        int elem_per_side = ceil(pow(space_size,1./dim));

        // make it even
        if(elem_per_side % 2 != 0) elem_per_side -= 1;

        for (unsigned int i = 0; i < number_of_levels - 1; ++i)
        {
            // auto coarse_mesh_ptr = make_shared<DistributedMesh>(init->comm());
            auto coarse_mesh_ptr = make_shared<DistributedMesh>(*comm);

            // check
            elem_per_side = (int) ceil(elem_per_side/coarsening_space);
            std::cout << "elem_per_side = " << elem_per_side << std::endl;

            if (elem_per_side < 2) std::cout << "WARNING: elem_per_side " << elem_per_side << " is too small on level " << i << std::endl;
            if (mpi_world_size() > pow(elem_per_side,dim)) std::cout << "WARNING: too many procs for level " << i << std::endl;

            if(dim == 3) {   /// (... / 2) * 2 to get closest even

                MeshTools::Generation::build_cube(*coarse_mesh_ptr,
                                              round(scaling_0 * elem_per_side / 2) * 2, round(scaling_1 * elem_per_side / 2) * 2, round(scaling_2 * elem_per_side / 2) * 2,
                                              box_min(0), box_max(0),
                                              box_min(1), box_max(1),
                                              box_min(2), box_max(2),
                                              HEX8);
            }
            else if (dim == 2)
            {
                MeshTools::Generation::build_square(*coarse_mesh_ptr,
                                                  round(scaling_0 * elem_per_side), round(scaling_1 * elem_per_side),
                                                  box_min(0), box_max(0),
                                                  box_min(1), box_max(1),
                                                  QUAD4);
            }
            else
            {
                MeshTools::Generation::build_line(*coarse_mesh_ptr,
                                                  round(scaling_0 * elem_per_side),
                                                  box_min(0), box_max(0),
                                                  EDGE2);
            }

            // to debug
            // ExodusII_IO mesh_out(*coarse_mesh_ptr);
            // mesh_out.write("../output/coarse_mesh_" + std::to_string(i)+".e");

            spatial_meshes.push_back(coarse_mesh_ptr);
        }
    }
}

void Space_timeMG::set_spatial_meshes(const vector<Mesh_ptr> &spatial_meshes_){

    if (number_of_levels - 1 != spatial_meshes_.size()) std::cout << "Input meshes vector has wrong size w.r.t. the coarsening strategy"<< std::endl;

    spatial_meshes = spatial_meshes_;
}

void Space_timeMG::set_coarse_operators(){

        if (spatial_ops.empty()) std::cerr << "WARNING: spatial_ops vector is empty, call set_spatial_ops()" << std::endl;

        coarse_operators.reserve(number_of_levels + 1); // also the base one should be here
        coarse_operators.push_back(space_time_op->get_space_time_system());

        // space
        int space_counter = 0;
        auto s_ass_ptr = spatial_ops[space_counter];

        // time
        auto dt = space_time_op->get_time_op()->get_time_step();
        auto time_steps = space_time_op->get_time_steps();
        auto t_ass_ptr = std::make_shared<Time_slab_assembler>(dt);

        for (unsigned int i = 0; i < number_of_levels; ++i)
        {
            // space coarsening
            s_ass_ptr = spatial_ops[i+1];
            s_ass_ptr->assemble();


            // time coarsening
            dt = coarsening_time * dt;
            time_steps = 1 + (time_steps - 1)/coarsening_time;

            // check: not too much time coarsening
            if (time_steps < 2 || (floor(time_steps) != time_steps)) std::cerr << "ERROR: coarse time steps on level " << i << " are " << time_steps << std::endl;

            t_ass_ptr = std::make_shared<Time_slab_assembler>(dt);
            t_ass_ptr->assemble();


            Space_time_assembler st_op_ptr(s_ass_ptr, t_ass_ptr, time_steps);
            st_op_ptr.suppress_flag_time_bc_warnings();
            st_op_ptr.assemble();

            coarse_operators.push_back(st_op_ptr.get_space_time_system());
        }

        std::reverse(coarse_operators.begin(), coarse_operators.end());
    }


void Space_timeMG::coupling(Space_assembler &fine_op, Space_assembler &coarse_op, utopia::USparseMatrix &interp_op, const bool transfer_residual)
{
    // moonolith::Communicator comm(init->comm().get());
    moonolith::Communicator comm(this->comm->get());
    USparseMatrix B;

    auto n_vars = fine_op.get_dof_map().n_variables();

    if(!utopia::assemble_volume_transfer(
                                         comm,
                                         coarse_op.get_mesh(),
                                         fine_op.get_mesh(),
                                         utopia::make_ref(coarse_op.get_dof_map()),
                                         utopia::make_ref(fine_op.get_dof_map()),
                                         0,
                                         0,
                                         true, // with false no problem with FE order ERORR
                                         n_vars,
                                         B,
                                         {},
                                         use_interpolation_)) //true if interp, false if projection
    {
        std::cerr << "No intersection" << std::endl;
        return;
    }

    UVector d;

    std::shared_ptr<USparseMatrix> Mass;

    if (transfer_residual)
    {
        d = sum(B, 1);
    }
    else
    {
        if (!coarse_op.get_mass())
        {
            coarse_op.assemble(); // TODO jassemble ust mass?
        }

        Mass = coarse_op.get_mass();
        d = sum(*Mass, 1);
    }

    UVector d_inv = local_values(local_size(d).get(0), 1.);

    {
        Write<UVector> w(d_inv);
        each_read(d, [&](const SizeType i, const double value) {
            if(value > 1e-15) {
                d_inv.set(i, 1./value);
            }
        });
    }

    USparseMatrix D_inv = diag(d_inv);

    if (transfer_residual)
    {
        interp_op = D_inv * B;
    }
    else{
        interp_op = B * D_inv;
    }

    // chop(interp_op, 1e-5);

    // pulizia tipo chop
    // UVector ones = local_values(local_size(interp_op).get(0), 1.0);

    // USparseMatrix T_t = transpose(interp_op);
    // UVector sum_T_cols = T_t * ones;

    // UScalar clamp_tol_ = 1e-6;

    // sum_T_cols.transform_values([clamp_tol_](const UScalar &val) -> UScalar {
    //     return (std::abs(val) < clamp_tol_) ? 1.0 : 0.0;
    // });

    // UScalar original_sum = sum(T_t);

    // set_zero_rows(T_t, sum_T_cols, 0.0);
    // interp_op = transpose(T_t);

    // UScalar removed_coarse_sum = sum(interp_op);
    // UScalar n_removed_dofs = sum(sum_T_cols);

    // if(sum_T_cols.comm().rank() == 0) {
    //     std::cout << original_sum << " - " << removed_coarse_sum << " (>) " << (original_sum > removed_coarse_sum) << std::endl;
    //     std::cout << std::abs(original_sum - removed_coarse_sum) <<  std::endl;
    //     std::cout << "n_removed: " << n_removed_dofs << "/" << sum_T_cols.size()  << std::endl;
    // }

    optimize_nnz(interp_op);

    // save_mat(raw_type(interp_op), "../output/P.m", "P_c");
}


// TODO 1D projection

PetscErrorCode Space_timeMG::coupling(const int time_steps_fine, const int time_steps_coarse, Mat &I)
{
    PetscErrorCode ierr;

    ierr = MatCreate(PETSC_COMM_WORLD, &I);CHKERRQ(ierr);
    ierr = MatSetSizes(I, PETSC_DECIDE, PETSC_DECIDE, time_steps_fine, time_steps_coarse);CHKERRQ(ierr);
    ierr = MatSetType(I,MATAIJ);CHKERRQ(ierr);
    ierr = MatSetUp(I);CHKERRQ(ierr);

    const double one = 1.0;

    if (coarsening_time == 1) // TODO to test
    {
        for (int i = 0; i < time_steps_fine; i++)
        {
            ierr = MatSetValues(I,1,&i,1,&i,&one,INSERT_VALUES);CHKERRQ(ierr);
        }
    }
    else if (coarsening_time == 2)
    {
        double v[2] = {0.5, 0.5};

        for (int i = 0; i < time_steps_fine; i++)
        {
            if (i % 2 == 0) // even
            {
                int col = i/2;
                ierr = MatSetValues(I,1,&i,1,&col,&one,INSERT_VALUES);CHKERRQ(ierr);
            }
            else // odd
            {
                int col[2] = {i/2, 1+i/2};
                ierr = MatSetValues(I,1,&i,2,col,v,INSERT_VALUES);CHKERRQ(ierr);
            }
        }
    }
    else if (coarsening_time == 4)
    {

        std::cout << "Coarsening in time = 4" << std::endl;

        double v[2] = {0.5, 0.5};
        double v1[2] = {3./4., 1./4.};
        double v2[2] = {1./4., 3./4.};

        for (int i = 0; i < time_steps_fine; i++)
        {
            if (i % 4 == 0)
            {
                int col = i/4;
                ierr = MatSetValues(I,1,&i,1,&col,&one,INSERT_VALUES);CHKERRQ(ierr);
            }
            else if (i % 4 == 1)
            {
                int col[2] = {i/4, 1+i/4};
                ierr = MatSetValues(I,1,&i,2,col,v1,INSERT_VALUES);CHKERRQ(ierr);
            }
            else if (i % 4 == 2)
            {
                int col[2] = {i/4, 1+i/4};
                ierr = MatSetValues(I,1,&i,2,col,v,INSERT_VALUES);CHKERRQ(ierr);
            }
            else
            {
                int col[2] = {i/4, 1+i/4};
                ierr = MatSetValues(I,1,&i,2,col,v2,INSERT_VALUES);CHKERRQ(ierr);
            }
        }
    }
    else{

        std::cout << "ERROR: coarsening_time = " << coarsening_time << " not supported " << std::endl;
        return 1;
    }

    ierr = MatAssemblyBegin(I,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(I,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

    return ierr;

}


PetscErrorCode Space_timeMG::set_interpolation_operators()
{
	PetscErrorCode ierr;

	auto time_op = space_time_op->get_time_op();
	auto time_slab_size = time_op->get_size();
	auto time_steps = space_time_op->get_time_steps();

	USparseMatrix I_time_slab = identity(time_slab_size,time_slab_size);

	int counter = 0;

	for (unsigned int i = 0; i < number_of_levels - 1; ++i)
	{
		USparseMatrix interp_op, interp_space_op, interp_space_op_slab, interp_time_op;

        // time coarsening
        int time_step_tmp = (time_steps - 1)/coarsening_time;
        int time_steps_coarse = (time_step_tmp < 1) ? 2 : 1 + time_step_tmp;
        if (time_steps == 1) time_steps_coarse = 1;

        if (mpi_world_rank() == 0 && (time_steps - 1)%coarsening_time != 0 && time_step_tmp > 1 )
            std::cerr << "ERROR: time steps on level " << i << " are not divisible by coarsening_time = " << coarsening_time <<  std::endl;

        ierr = coupling(time_steps, time_steps_coarse, raw_type(interp_time_op));CHKERRQ(ierr);
        time_steps = time_steps_coarse;

		// space coarsening
		auto & space_fine   = *spatial_ops[counter];
		auto & space_coarse = *spatial_ops[counter+1];
		counter++;

		coupling(space_fine, space_coarse, interp_space_op_slab);

        ierr = kron(raw_type(interp_space_op_slab), raw_type(I_time_slab), raw_type(interp_space_op));CHKERRQ(ierr);

        if ( i == 0 ) // fine level
        {
            ierr = kron(raw_type(interp_time_op), raw_type(interp_space_op), raw_type(interp_op), &raw_type(*space_time_op->get_space_time_system()));CHKERRQ(ierr);
        }
        else
        {
		    ierr = kron(raw_type(interp_time_op), raw_type(interp_space_op), raw_type(interp_op));CHKERRQ(ierr);
        }

        // save_mat(raw_type(interp_op), "../output/P_tot.m", "P_c");

		interpolation_operators.push_back(make_shared<USparseMatrix>(interp_op));
	}

    // because of the change in utopia MG we need to reverse the order
    std::reverse(interpolation_operators.begin(),interpolation_operators.end());

	return ierr;
}

// TODO: this two methods need to be tested
void Space_timeMG::solve(UVector &solution, bool verbose_flag){

    if (init_flag_)
    {
        multigrid->verbose(verbose_flag);
        multigrid->update(space_time_op->get_space_time_system());
        multigrid->apply(*space_time_op->get_space_time_rhs(), solution);

        // save_mat(raw_type(*interpolation_operators[0]), "../output/Transfer.m", "T");
        // save_mat(raw_type(*interpolation_operators[1]), "../output/Transfer_cr.m", "T2");

        // PetscErrorCode ierr;

        // // ierr = KSPSetUp(multigrid);CHKERRQ(ierr);
        // ierr = KSPSolve(multigrid,raw_type(*space_time_op->get_space_time_rhs()), raw_type(solution));CHKERRV(ierr);

        // // destroy
        // ierr = KSPDestroy(&multigrid);CHKERRV(ierr);
    }
    else{
        std::cout << "WARNING: MG was not initialized, call init_mg()!" << std::endl;
    }
}

void Space_timeMG::print_mu(double diffusivity) const {

    if(utopia::mpi_world_rank() == 0) {

        double mu = 0.0;

        // dt at fine level
        auto dt = space_time_op->get_time_op()->get_time_step();

        for (unsigned int i = 0; i < number_of_levels - 1; ++i)
        {
            int counter = 0;
            double dx = 0.0;

            auto mesh = spatial_meshes[i];

            for(auto e_it = mesh->local_elements_begin();
                    e_it != mesh->local_elements_end();
                    ++e_it) {

                dx += ((**e_it).hmax() + (**e_it).hmin())/2.;

                counter++;
            }

            dx = dx/counter;

            double dt_coarse = dt * pow(coarsening_time, i + 1);

            mu = diffusivity * dt_coarse / (dx * dx);

            std::cout << "" << std::endl;
            std::cout << ".-----------------------" << std::endl;
            std::cout << "| mu on level " << i + 1  << " = " << mu <<  std::endl;
            std::cout << "'-----------------------" << std::endl;
            std::cout << "" << std::endl;

            if (mu > 10 || mu < 0.1)  std::cout << "WARNING: 0.1 < mu < 10 for better MG convergence!" << std::endl;
        }
    }
}




