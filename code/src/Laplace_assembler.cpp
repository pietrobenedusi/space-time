#include "Laplace_assembler.hpp"
#include "Utilities.hpp"
#include "utopia_UIScalarSampler.hpp"

using namespace utopia;
using namespace libMesh;
using std::make_shared;

//TODO this can be removed: check in test if it is used (prob not)
Laplace_assembler::Laplace_assembler(LibMeshInit &init, const std::string mesh_path, bool bc_term_, double diff_coeff_):
		bc_term(bc_term_), diff_coeff_scalar(diff_coeff_){

	// init mesh
	mesh = make_shared<DistributedMesh>(init.comm());
	mesh->read(mesh_path);

	initialize();
}

Laplace_assembler::Laplace_assembler(Mesh_ptr mesh_, bool bc_term_, double diff_coeff_):
		bc_term(bc_term_), diff_coeff_scalar(diff_coeff_){

	mesh = mesh_;

	initialize();
}

void Laplace_assembler::initialize(){

	// set spaces
	auto equation_systems = std::make_shared<libMesh::EquationSystems>(*mesh);
	equation_systems->add_system<libMesh::LinearImplicitSystem>("space");

	// set FEM space
	V = std::make_shared<LibMeshFunctionSpace>(equation_systems);

	// set BC
	if (bc_term)
	{
		auto constr = constraints(boundary_conditions(trial(*V) == coeff(0.), {1})); // TODO, hardcoded here
		init_constraints(constr);
	}

	V->initialize();

	// set size
	size_space = equation_systems->n_dofs();

	// set dime
	dim = V->mesh().spatial_dimension();

	//set diffusion
	// auto &dof_map = V->dof_map(); // old way remove when tested enough
	// diff_coeff = std::make_shared<UVector>(ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), dof_map.get_send_list()));
 	// diff_coeff->set(diff_coeff_scalar);
	set_diff_coeff(diff_coeff_scalar);
}

void Laplace_assembler::assemble(){

	// init matrices
	if(!mass)  { mass = std::make_shared<USparseMatrix>();  }
	if(!stiff) { stiff = std::make_shared<USparseMatrix>(); }
	if(!rhs)   { rhs = std::make_shared<UVector>(); 	}

	auto u = trial(*V);
    auto v = test(*V);
    
    // Set linear forms
	auto linear_form = inner(coeff(forcing_term), v) * dX;
	auto mass_form = inner(u, v) * dX;

	// assemble  
    utopia::assemble(linear_form, *rhs);
    utopia::assemble(mass_form, *mass);

    // Set bilinear form, depending on diffusion
    if (diff_tensor_flag)    	
    {    
    	auto D = tensor_interpolate(*V, *diff_coeff, dim, dim);    	
        auto bilinear_form = inner(D * grad(u), grad(v)) * dX;
        utopia::assemble(bilinear_form, *stiff);
        
    } else { 

    	if (diff_jump_flag)
    	{
    		auto d = ctx_fun(lambda_fun([](const std::vector<double> &p) -> double {

    				double c = sin(8 * p[1]) * sin(8 * p[0]);
    				double th = 0.05;

                    if(c < th && c > - th) {
                        return 1.0;
                    }
                    else {
                        return 0.001;
                    }
            }));

            auto bilinear_form = inner(d * grad(u), grad(v)) * dX;
            utopia::assemble(bilinear_form, *stiff);
    	}
    	else{
	    	auto d = interpolate(*diff_coeff, u);
	    	auto bilinear_form = inner(d * grad(u), grad(v)) * dX;
	    	utopia::assemble(bilinear_form, *stiff);
    	}
    }

	if (bc_term)
	{
    	apply_boundary_conditions(V->dof_map(), *stiff, *rhs);     
		set_zero_at_constraint_rows(V->dof_map(), *mass); // TODO this hardcoded
    }	


    if (mass_lumping)
    {
    	std::cout << "Lumping mass matrix..." << std::endl;
    	*mass = diag(sum(*mass,1));
    }
}
