#include "Eikonal_assembler.hpp"
#include "Utilities.hpp"

using namespace utopia;
using namespace libMesh;
using std::make_shared;

Eikonal_assembler::Eikonal_assembler(Mesh_ptr mesh_, int boundary_tag, double tau, double c0, double diff_coeff_){

	mesh = mesh_;

	// set spaces
	auto equation_systems = std::make_shared<libMesh::EquationSystems>(*mesh);
	equation_systems->add_system<libMesh::LinearImplicitSystem>("space");

	V = std::make_shared<LibMeshFunctionSpace>(equation_systems);

	// set parameters
	c1 = 1./tau;
	c2 = c1 * c0;

	// set BC
	auto constr = constraints(boundary_conditions(trial(*V) == coeff(0.), {boundary_tag}));
	init_constraints(constr);

	V->initialize();

	//set diffusion
	auto &dof_map = V->dof_map();
	utopia::UIndexArray ghost_nodes;
	convert(dof_map.get_send_list(), ghost_nodes);

	diff_coeff = std::make_shared<UVector>(ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes));
    diff_coeff->set(diff_coeff_);
}

void Eikonal_assembler::solve(double initial_guess, int non_linear_its){

	// init matrices
	if(!hessian) { hessian = std::make_shared<USparseMatrix>(); }
	if(!gradient){ gradient = std::make_shared<UVector>(); }

	// init sol
	auto &dof_map = V->dof_map();
	dof_map.prepare_send_list();

	utopia::UIndexArray ghost_nodes;
	convert(dof_map.get_send_list(), ghost_nodes);

	UVector sol = ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes);
	auto d_sol = sol;
	d_sol.set(0.);
	sol.set(initial_guess);

	//assemble operators
	auto du = trial(*V);
    auto v = test(*V);

    auto d = interpolate(*diff_coeff, du);

    //set solver
    // GMRES<USparseMatrix, UVector> solver;
    Factorization<USparseMatrix, UVector> solver;

    double diff = 0;

	for (int i = 0; i < non_linear_its; ++i)
	{
		utopia::synchronize(sol);

	    auto u_old = interpolate(sol, du);

		// Set (bi)linear forms
		auto l_form = c1 * inner(d * grad(u_old), grad(v)) * dX
			          + c2 * inner(sqrt(inner(grad(u_old), grad(u_old))), v) * dX
			          - inner(coeff(forcing_term), v) * dX;

		// minus because of newton iteration
		auto b_form = c1 * inner(d * grad(du), grad(v)) * dX
		 			+ c2 * inner(inner(grad(du),grad(u_old))/(coeff(1e-10) + sqrt(inner(grad(u_old), grad(u_old)))), v) * dX;

		// assemble
	    utopia::assemble(b_form, *hessian);
	    utopia::assemble(l_form, *gradient);

	    (*gradient) *= -1.;

    	apply_boundary_conditions(V->dof_map(), *hessian, *gradient);

    	if(i > 0) {
    		apply_zero_boundary_conditions(V->dof_map(),  *gradient);
    	}

		solver.update(hessian);
		solver.apply(*gradient, d_sol);

		diff = norm2(d_sol);

		std::cout << "--------------\n";
		disp(diff);

		if (diff < 1e-10)
		{
			break;
		}

		// correction
		sol = sol + d_sol;
	}

	solution = make_shared<utopia::UVector>(sol);

	convert(sol, *V->equation_system().solution);
}
