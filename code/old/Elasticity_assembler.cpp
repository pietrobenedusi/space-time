#include "Elasticity_assembler.hpp"
#include "Utilities.hpp"

using namespace utopia;
using namespace libMesh;
using std::make_shared;

Elasticity_assembler::Elasticity_assembler(LibMeshInit &lm_init, const std::string mesh_path, const bool bc_term)
: bc_term(bc_term)
{
	mesh = make_shared<DistributedMesh>(lm_init.comm());
	mesh->read(mesh_path);
	init();
}

Elasticity_assembler::Elasticity_assembler(Mesh_ptr mesh_, const bool bc_term)
: bc_term(bc_term)
{
	mesh = mesh_;
	init();
}

void Elasticity_assembler::init() {
	std::string names[3] = {"disp_x", "disp_y", "disp_z"};

	auto dim = mesh->mesh_dimension();

	// set spaces
	auto equation_systems = std::make_shared<libMesh::EquationSystems>(*mesh);
	equation_systems->add_system<libMesh::LinearImplicitSystem>("elasticity");

	V = std::make_shared<ProductFunctionSpaceT>();

	for(SizeType i = 0; i < dim; ++i) {
		V->add_subspace(std::make_shared<FunctionSpaceT>(equation_systems, libMesh::LAGRANGE, libMesh::FIRST, names[i]));
	}

	// set size
	size_space = equation_systems->n_active_dofs(); // active?

	// set BC
	if(bc_term)
	{
		auto u = trial(*V);
		auto ux = u[0];
		auto uy = u[1];

		auto constr = constraints(
			boundary_conditions(ux == coeff(0.0), {2}),
			boundary_conditions(uy == coeff(0.0), {2})
		);

		if(dim == 3) {
			auto uz = u[0];
			init_constraints(constr + boundary_conditions(uz == coeff(0.), {2}));
		} else {
			init_constraints(constr);
		}
	}

	V->subspace(0).initialize();

	size_space = V->subspace(0).dof_map().n_dofs();
}

void Elasticity_assembler::assemble(){

	// init matrices
	if(!mass)  { mass  = std::make_shared<USparseMatrix>(); }
	if(!stiff) { stiff = std::make_shared<USparseMatrix>(); }
	if(!rhs)   { rhs   = std::make_shared<UVector>(); 	}

	auto u = trial(*V);
    auto v = test(*V);

    // Set (bi)linear forms
	auto mu     = 10.;
	auto lambda = 10.;

	auto e_u = 0.5 * ( transpose(grad(u)) + grad(u) );
	auto e_v = 0.5 * ( transpose(grad(v)) + grad(v) );

	auto b_form = integral((2. * mu) * inner(e_u, e_v) + lambda * inner(div(u), div(v)));

	auto vx = v[0];
	auto vy = v[1];

	// auto l_form = (inner(coeff(-2.), vy) + inner(coeff(3.), vx)) * dX;
	auto l_form = (inner(coeff(-2.), vy)) * dX;
	auto mass_form = inner(u, v) * dX;

	// assemble
    utopia::assemble(b_form, *stiff);
    utopia::assemble(l_form, *rhs);
    utopia::assemble(mass_form, *mass);

	if(bc_term) {
		apply_boundary_conditions(V->subspace(0).dof_map(), *stiff, *rhs);
	}

	// TODO this hardcoded
	set_zero_at_constraint_rows(V->subspace(0).dof_map(), *mass);
}
