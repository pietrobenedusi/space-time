#ifndef Eikonal_assembler_hpp
#define Eikonal_assembler_hpp

#include "utopia.hpp"
#include "utopia_fe_core.hpp"
#include "utopia_LibMeshBackend.hpp"
#include "libmesh/parallel_mesh.h"
#include "utopia_libmesh_NonLinearFEFunction.hpp"

class Eikonal_assembler{

    typedef std::shared_ptr<utopia::USparseMatrix> Matrix;
    typedef std::shared_ptr<utopia::UVector>  Vector;
    typedef std::shared_ptr<libMesh::DistributedMesh> Mesh_ptr;

public:
    Eikonal_assembler(Mesh_ptr mesh_, int boundary_tag, double tau, double c0, double diff_coeff_ = 1.);

    void solve(double initial_guess = 0., int non_linear_its = 20);

    // setters and getters
    inline void set_diff_coeff(Vector diff_coeff_){diff_coeff = diff_coeff_;}
    inline void set_diff_coeff(double diff_coeff_){
        auto &dof_map = V->dof_map();

        utopia::UIndexArray ghost_nodes;
        utopia::convert(dof_map.get_send_list(), ghost_nodes);

        diff_coeff = std::make_shared<utopia::UVector>(utopia::ghosted(dof_map.n_local_dofs(), dof_map.n_dofs(), ghost_nodes));
        diff_coeff->set(diff_coeff_);
    }

    inline void set_forcing_term(const double force){forcing_term = force;}

    inline Vector get_solution() const{return solution;}

private:

    Mesh_ptr mesh;

    double forcing_term = 1.;

    // problem parameters
    Vector diff_coeff;

    double c1; // 1/tau
    double c2; // c0*c1 = c0/tau

    std::shared_ptr<utopia::LibMeshFunctionSpace> V;

    // assembled operators // TODO, this are not needed here?
    Matrix  hessian;
    Vector gradient;

    // solution
    Vector solution;
};

#endif



