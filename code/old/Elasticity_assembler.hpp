#ifndef Elasticity_assembler_hpp
#define Elasticity_assembler_hpp

#include "Space_assembler.hpp"

class Elasticity_assembler : public Space_assembler {

    typedef std::shared_ptr<utopia::USparseMatrix> Matrix;
    typedef std::shared_ptr<utopia::UVector>  Vector;
    typedef std::shared_ptr<libMesh::DistributedMesh> Mesh_ptr;
    typedef utopia::LibMeshFunctionSpace FunctionSpaceT;
    typedef utopia::ProductFunctionSpace<FunctionSpaceT> ProductFunctionSpaceT;

public:
    Elasticity_assembler(libMesh::LibMeshInit &init, const std::string mesh_path, const bool bc_term);
    Elasticity_assembler(Mesh_ptr mesh_, const bool bc_term);

    void assemble();
    void init();

    inline utopia::SizeType get_size() const{ return size_space; }
    inline std::shared_ptr<ProductFunctionSpaceT> get_fem_space() const{ return V; }
    inline libMesh::System & get_equation_system() const{ return V->subspace(0).equation_system();}
    inline libMesh::EquationSystems & get_equation_systems() const{ return V->subspace(0).equation_systems();}

    libMesh::DofMap & get_dof_map() const{ return V->subspace(0).dof_map();}

private:
    std::shared_ptr<ProductFunctionSpaceT> V;
    utopia::SizeType size_space;
    bool bc_term;
};

#endif



